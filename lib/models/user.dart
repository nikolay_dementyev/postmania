import 'package:firebase_uploading/models/base_model.dart';

class User extends BaseModel {
  
  String name;
  String email;
  
  User({this.name, this.email});
  
  @override
  BaseModel copyWith() {
    return User(email: this.email, name: this.name);
  }
}