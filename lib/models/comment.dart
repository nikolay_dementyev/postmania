import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_uploading/utils/firebase/firestore.dart';

class Comment {
  
  /// Firebase-Related Variables
  String id;
  String postId;
  String parentId;
  String userId;
  String comment;
  Timestamp timestamp;
  List<String> likedUsers;
  List<String> dislikedUsers;
  
  /// Code-Related Variables
  String oppositeName;
  String name;
  String avatar;
  String createdTime;
  List<Comment> replyComments = [];
  
  Comment({this.id, this.postId, this.parentId, this.userId, this.comment}) {
    likedUsers = [];
    dislikedUsers = [];
    replyComments = [];
  }
  
  Comment._internalFromJson(Map jsonMap) {
    id = jsonMap['id'];
    postId = jsonMap['postId'];
    parentId = jsonMap['parentId'];
    userId = jsonMap['userId'];
    comment = jsonMap['comment'];
    oppositeName = jsonMap['oppositeName'];
    timestamp = jsonMap['timestamp'];
    likedUsers = (jsonMap['likedUsers'] as List).map<String>((item) => item).toList();
    dislikedUsers = (jsonMap['dislikedUsers'] as List).map<String>((item) => item).toList();
    
    createdTime = FireStore.getTimeStampDiff(timestamp);
  }
  
  Map<String, dynamic> toMap({bool isUpdate = false}) {
    
    Map<String, dynamic> map = {
      'id': id,
      'postId': postId,
      'parentId': parentId,
      'oppositeName': oppositeName,
      'userId': userId,
      'avatar' : avatar,
      'comment' : comment,
      'likedUsers': likedUsers ?? [],
      'dislikedUsers': dislikedUsers ?? [],
      'timestamp' : FieldValue.serverTimestamp()
    };
    if(isUpdate) {
      map.remove('timestamp');
    }
    return map;
  }
  
  factory Comment.fromJson(Map jsonMap) => Comment._internalFromJson(jsonMap);
}