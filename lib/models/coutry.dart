class Country {

  String name;
  String capital;
  String region;
  int population;
  String nativeName;
  String flag;
  
  Country(
      {this.name,
      this.capital,
      this.region,
      this.population,
      this.nativeName,
      this.flag});
  
  Country.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    capital = json['capital'];
    region = json['region'];
    population = json['population'];
    nativeName = json['nativeName'];
    flag = json['flag'];
  }
  
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['capital'] = this.capital;
    data['region'] = this.region;
    data['population'] = this.population;
    data['nativeName'] = this.nativeName;
    data['flag'] = this.flag;
    return data;
  }
}