import 'base_model.dart';

class Profile extends BaseModel {
  
  String userId;
  String email;
  String firstName;
  String lastName;
  String userName;
  String password;
  String avatarUrl;
  String aboutMe;
  String cityName;
  String stateName;
  String countryName;
  
  Profile({this.userId, this.avatarUrl, this.cityName, this.stateName, this.countryName, this.aboutMe, this.firstName, this.lastName, this.userName, this.password, this.email});
  
  Map toMap() {
    return <String, dynamic>{
      'user_id' : userId,
      'user_name' : userName,
      'avatar' : avatarUrl,
      'about_me' : aboutMe,
      'city' : cityName,
      'state' : stateName ?? 'Califonia',
      'first_name' : firstName,
      'last_name' : lastName,
      'country' : countryName
    };
  }
  
  Profile._internalFromJson(Map jsonMap) {
    userId = jsonMap['user_id'];
    userName = jsonMap['user_name'];
    avatarUrl = jsonMap['avatar'];
    cityName = jsonMap['city'];
    aboutMe = jsonMap['about_me'];
    firstName = jsonMap['first_name'];
    lastName = jsonMap['last_name'];
    stateName = jsonMap['state'];
    countryName = jsonMap['country'];
  } 
  
  factory Profile.fromJson(Map jsonMap) => Profile._internalFromJson(jsonMap);
  
  @override
  BaseModel copyWith() {
    Profile profile = Profile(userId: this.userId, avatarUrl: this.avatarUrl, cityName: this.cityName, countryName: this.countryName, stateName: stateName, firstName: firstName, lastName: lastName, aboutMe: this.aboutMe, userName: this.userName, email: this.email);
    
    return profile;
  }
}