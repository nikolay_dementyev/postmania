class Address {
    String name;
    String streetLine;
    String city;
    String state;
    
    Address._internalFromJson(Map jsonMap)
        : name = jsonMap['text'] as String,
          streetLine = jsonMap['street_line'] as String,
          city = jsonMap['city'] as String,
          state = jsonMap['state'] as String;
    factory Address.fromPrefsJson(Map jsonMap) => Address._internalFromJson(jsonMap);
}