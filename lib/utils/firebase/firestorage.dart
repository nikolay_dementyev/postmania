import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_uploading/helpers/index.dart';
import 'package:firebase_uploading/singletons/global.dart';

class FireStorage {
  
  static Future<String> _saveFile(File file) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('No Internet');
      return null;
    }
    try {
      String fileName = file.path;
      fileName = fileName.split('/').last;
      fileName += randomString(length: 10);
      StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(fileName);
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(file);
      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
      final String url = await taskSnapshot.ref.getDownloadURL();
      return url;
    } catch (e) {
      return null;
    }
  }
  
  static Future<String> saveAttachFile(File file) async {
    return await _saveFile(file);
  }
  
  static Future deleteFile(String url) async {
    if(url == null || url.isEmpty) return;
    StorageReference storageReference = await FirebaseStorage.instance.getReferenceFromUrl(url);
    if(storageReference == null) return;
    await storageReference.delete();
  }  
}