import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/models/comment.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import 'package:meta/meta.dart';

class FireStore {
  
  static final fireStore = Firestore.instance;
  static const String POST_COLLECTION = 'posts';
  static const String COMMENT_COLLECTION = 'comments';
  
  static Future<Post> addPost({PostType postType, String posterId, String posterName, String title, String country, String state, String city, String address, String thumbUrl, String url}) async {
   if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
   try {
    var documentSnapshot = await fireStore.collection(POST_COLLECTION).add({});
    Post newPost = Post();
    newPost.id = documentSnapshot.documentID;
    newPost.description = title;
    newPost.postType = postType;
    newPost.country = country;
    newPost.region = state;
    newPost.city = city;
    newPost.address = address;
    newPost.thumbnailUrl = thumbUrl;
    newPost.posterId = posterId;
    newPost.postUrl = url;
    newPost.posterName = posterName;
    await fireStore.collection(POST_COLLECTION).document(newPost.id).updateData(
     newPost.toMap()
    );
    return newPost;
   } catch (e) {
     print(e.toString());
     return null;
   }
  }
  
  static Future<bool> deletePost(Post post) async {
    try {
      await FireStorage.deleteFile(post.postUrl);
      if(post.postType == PostType.Video) {
        await FireStorage.deleteFile(post.thumbnailUrl);
      }
      await fireStore.collection(POST_COLLECTION).document(post.id).delete();
      return true;
    } catch (e) {
      return false;
    }
  }
  
  static Future updatePost(Post post)  async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
      await fireStore.collection(POST_COLLECTION).document(post.id).updateData(
        post.toMap(isUpdate: true)
      );
    } catch (e) {
      print(e.toString());
    }
  }
  
  static String getTimeStampDiff(Timestamp past) {
    DateTime now = DateTime.now();
    DateTime pastDateTime = past.toDate();
    pastDateTime = pastDateTime.toLocal();
    Duration duration = now.difference(pastDateTime); 
    int secondsDiff = duration.inSeconds;
    if(secondsDiff < 60) {
      return '1min ago';
    }
    
    int minutesDiff = duration.inMinutes;
    if(minutesDiff < 60) {
      return '$minutesDiff mins ago';
    }
    
    int hourDiff = duration.inHours;
    if(hourDiff < 24) {
      return '$hourDiff hours ago';
    }
    
    int daysDiff = duration.inDays;
    if(daysDiff < 30) {
      return '$daysDiff days ago';
    } else if(daysDiff < 365) {
      return '${daysDiff / 30} months ago';
    } else {
      return '1 year ago';
    }
  }
  
  static Future<List<Comment>> fetchComments(String postId) async {
      if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
      QuerySnapshot querySnapshot = await fireStore.collection(POST_COLLECTION).document(postId).collection(COMMENT_COLLECTION).orderBy('timestamp', descending: false).getDocuments();
      if(querySnapshot.documents.isEmpty) {
        return null;
      }
      return querySnapshot.documents.map<Comment>((document) => Comment.fromJson(document.data)).toList();
    } catch (e) {
      return null;
    }    
  }
  
  static Future<Comment> addComment(Comment comment) async {
      if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
      DocumentReference documentReference = await fireStore.collection(POST_COLLECTION).document(comment.postId).collection(COMMENT_COLLECTION).add({});
      comment.id = documentReference.documentID;
      await documentReference.updateData(comment.toMap());
      comment.createdTime = '1 min ago';
      return comment;
    } catch (e) {
      return null;
    }
  } 
  
  static Future<Comment> updateComment(Comment comment) async {
      if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
      await fireStore.collection(POST_COLLECTION).document(comment.postId).collection(COMMENT_COLLECTION).document(comment.id).updateData(comment.toMap(isUpdate: true));
      print('object');
      return comment;
    } catch (e) {
      return null;
    }
  }
  
  static Future<bool> createUserDetailDocument(Profile profile) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return false;
    }
    try {
      await fireStore.collection('user_details').document(profile.userId).setData(profile.toMap());
      return true;
    } catch (e) {
      return false;
    }
  } 
  
  static Future<bool> updateUserDetail(Profile profile) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return false;
    }
    try {
      await fireStore.collection('user_details').document(profile.userId).setData(profile.toMap());
      return true;
    } catch (e) {
      return false;
    }
  }
  
  static Future<Profile> getUserDetail(String userId) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    var documentSnapshot = await fireStore.collection('user_details').document(userId).get();
    if(documentSnapshot.data == null) {
      return null;
    }
    return Profile.fromJson(documentSnapshot.data);
  }
  
  static Future<List<Post>> getMyPosts(String userId) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    var documentSnapshot = await fireStore.collection('posts').orderBy('timestamp', descending: true).getDocuments();
    if(documentSnapshot.documents.length > 0) {
      List<Post> messageList = [];
      for(int i = 0; i < documentSnapshot.documents.length; i++) {
        Post message = Post.fromJson(documentSnapshot.documents[i].data);
        message.createdTime = getTimeStampDiff(message.timeStamp);
        message.me = true;
        messageList.add(message);
      }
      messageList.retainWhere((item) => item.posterId == userId);
      return messageList;
    } else {
      return null;
    }
  }
  
  static Future<List<Post>> fetchPosts(String query) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    var documentSnapshot = await fireStore.collection('posts').orderBy('timestamp', descending: true).getDocuments();
    if(documentSnapshot.documents.length > 0) {
      List<Post> messageList = [];
      for(int i = 0; i < documentSnapshot.documents.length; i++) {
        Post message = Post.fromJson(documentSnapshot.documents[i].data);
        message.createdTime = getTimeStampDiff(message.timeStamp);
        messageList.add(message);
      }
      messageList.retainWhere((item) => item.description.toLowerCase().contains(query.toLowerCase()) || item.country.toLowerCase().contains(query.toLowerCase()) || item.city.toLowerCase().contains((query.toLowerCase())));
      return messageList;
    } else {
      return null;
    }
  }
  
  static StreamSubscription addPostChangeListener({@required Function onPostAdded}) {
   
    return fireStore.collection(POST_COLLECTION).snapshots().listen((snapShot) {
      if(snapShot.documentChanges.isNotEmpty) {
        for(int i = 0; i < snapShot.documentChanges.length; i++) {
          var change = snapShot.documentChanges[i];
          if(change.type == DocumentChangeType.modified) {
            onPostAdded(Post.fromJson(change.document.data));
          }
        }
      }
    });
  }
}