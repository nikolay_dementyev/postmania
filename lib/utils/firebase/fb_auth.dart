import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/singletons/global.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'firestore.dart';

class FbAuthManager {
  
  FirebaseAuth _auth = FirebaseAuth.instance;
  
  static final FbAuthManager _instance = FbAuthManager._internal();
  
  FbAuthManager._internal();
  
  factory FbAuthManager() {
    return _instance;
  }
  
  // Log with email and password
  Future<Profile> signIn(String email, String password) async {
     if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
      AuthResult authResult = await _auth.signInWithEmailAndPassword(email: email, password: password); 
      FirebaseUser newUser = authResult.user;
      return Profile(userId: newUser.uid, email: newUser.email, userName: newUser.displayName);
    } catch (e) {
      Global().showToastMessage(e.message);
      return null;
    }
  }
  
  // Register with email and password
  Future<Profile> signUp(Profile profile) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    try {
     AuthResult res = await _auth.createUserWithEmailAndPassword(email: profile.email, password: profile.password);
     FirebaseUser newUser = res.user;
     profile.userId = newUser.uid;
     profile.aboutMe = 'I am a ...';
     profile.avatarUrl = null;
     profile.cityName = 'Mountain View';
     profile.countryName = 'United States';
     profile.firstName = profile.userName.split('/').first;
     profile.lastName = profile.userName.split('/').length > 1 ? profile.userName.split('')[1] : '';
     bool isCreated = await FireStore.createUserDetailDocument(profile);
     
     if(isCreated) {
       return profile;
     } else {
       return null;
     }
    } catch (e) {
      Global().showToastMessage(e.message);
      return null;
    }
  }
  
  /// Sign out
  Future<bool> signOut() async {
    try {
      await _auth.signOut();
      return true;
    } catch (e) {
      Global().showToastMessage(e.message);
      return false;
    }
  }
   
  /// Future
  Future<Profile> signInWithGoogle() async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return null;
    }
    GoogleSignIn googleSignIn = GoogleSignIn();
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
    
    AuthCredential authCredential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken
    );
    
    AuthResult authResult = await _auth.signInWithCredential(authCredential);
    FirebaseUser newUser = authResult.user;
    Profile profile = await FireStore.getUserDetail(newUser.uid);
    
    if(profile != null) {
      profile.email = newUser.email;
      return profile;
    }
    
    profile = Profile();
    profile.userId = newUser.uid;
    profile.aboutMe = 'I am a ...';
    profile.avatarUrl = null;
    profile.cityName = 'New York';
    profile.countryName = 'United States';
    profile.userName = newUser.displayName;
    profile.email = newUser.email;
    bool isCreated = await FireStore.createUserDetailDocument(profile);
    if(isCreated) {
      return profile;
    } else {
      return null;
    }
  }
}
