import 'base_db_client.dart';

class LocationDBClient extends BaseDBClient {
  
  Future<List<String>> getCountries() async {
    List<Map> data = await runQuery('select * from country;');
    return data.map<String>((jsonMap) => jsonMap['country_name']).toList();
  }
  
  Future<List<String>> getStates(String country) async {
    List<Map> data = await runQuery('SELECT * FROM state where country_id IN (SELECT id FROM country WHERE country_name = "$country");');
    return data.map<String>((jsonMap) => jsonMap['state_name']).toList();
  }
  
  Future<List<String>>  getCities(String stateName) async {
    List<Map> data = await runQuery('SELECT * FROM city where state_id IN (SELECT id FROM state WHERE state_name = "$stateName");');
    return data.map<String>((jsonMap) => jsonMap['city_name']).toList();
  }
  
  Future<int> getCountryIdByName(String countryName) async {
    List<Map> countries = await runQuery('SELECT id from country where country_name = "$countryName";');
    if(countries.isEmpty) {
      return -1;
    }
    return countries[0]['id'];
  }
  
  Future<int> getStateIdByName(String stateName) async {
    List<Map> cities = await runQuery('SELECT id from state where state_name = "$stateName";');
    if(cities.isEmpty) {
      return -1;
    }
    return cities[0]['id'];
  }
  
  Future<bool> saveCountry(String country) async {
    try {
      await runQuery('INSERT INTO country (country_name) VALUES ("$country");');
      return true;
    } catch (e) {
      return false;
    }
  }
  
  Future<bool> saveState(int countryId, String stateName) async {
    try {
      await runQuery('INSERT INTO state (country_id, state_name) VALUES ($countryId, "$stateName");');
      return true;
    } catch (e) {
      return false;
    }
  }
  
  Future<bool> saveCity(int stateId, String cityName) async {
    try {
      await runQuery('INSERT INTO city (state_id, city_name) VALUES ($stateId, "$cityName");');
      return true;
    } catch (e) {
      return false;
    }
  }
}