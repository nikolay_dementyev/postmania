import 'package:path_provider/path_provider.dart';
import 'base_db_client.dart';
import 'package:firebase_uploading/app/post/bloc/post_model.dart';
import 'dart:io';

class PostDBClient extends BaseDBClient {
  Future addPost({
    PostType type, 
    String posterId, 
    String posterName, 
    String title, 
    String country, 
    String state, 
    String city, 
    String address, 
    File contentFile,
    File thumbFile
  }) async {
    String path = (await getApplicationDocumentsDirectory()).path;
    String contentFileName = contentFile.path.split('/').last;
    contentFileName = path + "/" + DateTime.now().toString() + contentFileName;
    
    File newContentFile = File(contentFileName);
    if(type == PostType.Image) {
      await newContentFile.writeAsBytes(contentFile.readAsBytesSync());
    } 
    
    String thumbFileName = thumbFile.path.split('/').last;
    thumbFileName = path + "/" + DateTime.now().toString() + thumbFileName;
    File newThumbFile = File(thumbFileName);
    await newThumbFile.writeAsBytes(thumbFile.readAsBytesSync());
    
    await runQuery('INSERT INTO post (type, posterId, posterName, title, country, state, city, address, contentFilePath, thumbFilePath) VALUES ("${type == PostType.Image ? 'image' : 'video'}", "$posterId", "$posterName", "$title", "$country", "$state", "$city", "$address", "$contentFileName", "$thumbFileName");');
    print(path);
  }
  
  Future<List<Post>> fetchPosts() async {
    List<Map> data = await runQuery('SELECT * FROM post;');
    return data.map<Post>((jsonMap) => Post.fromDbJson(jsonMap)).toList();
  }
  
  Future deletePost(int id) async {
    await runQuery('DELETE from post WHERE id = $id;');
  }
}