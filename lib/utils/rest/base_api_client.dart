import 'dart:convert';
import 'package:http/http.dart';

abstract class BaseApiClient {
  
  Future<dynamic> getRequest(String url, {Map<String, String> headers}) async {
    try {
      final response = await get(url, headers: headers);
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  
  Future<dynamic> postRequest(String url, Map<String, String> headers, Map body) async {
    
    try {
      final response = await post(url, 
          headers: headers,
          body: body
      );
      
      final jsonData = json.decode(response.body); 
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  
  Future<dynamic> deleteRequest(String url, Map<String, String> headers) async {
    try {
      final response = await delete(url, headers: headers);
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  } 
  
  Future<dynamic> putRequest(String url, Map<String, String> headers, Map body) async {
    try {
      final response = await put(url,
        headers: headers,
        body: body
      );
      
      final jsonData = json.decode(response.body);
      return jsonData;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}