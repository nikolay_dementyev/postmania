import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'base_api_client.dart';

class LocationApiClient extends BaseApiClient {
  
  static String _authToken;
  
  static const String apiToken = 'GIUt_N3Ajgomh0USLwLxSnr5-unH--qwRXSm03ZfOkeY82JGR54qJeQ2kumxbuNQHkQ';
  static const String email = 'Amal.Ksenia44@gmail.com'; 
  
  Future<String> authenticate() async {
    String url = 'https://www.universal-tutorial.com/api/getaccesstoken';
    var res = await getRequest(url, headers: {
      "Accept": "application/json",
      "api-token": apiToken,
      "user-email": email
    });
    
    if(res == null) return null;

    return res['auth_token'];
  }
  
  _getHeader() {
    return {
      "Authorization": "Bearer $_authToken",
      "Accept": "application/json"
    };
  }
  
  Future<List<String>> getCountries() async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return [];
    }
    if(_authToken == null) {
      _authToken = await authenticate();
    }
    String url = 'https://www.universal-tutorial.com/api/countries';    
    List countryData = await getRequest(url, headers: _getHeader());
    return countryData.map<String>((jsonMap) => jsonMap['country_name']).toList();
  }
  
  Future<List<String>> getRegions(String country) async {
    if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return [];
    }
    if(_authToken == null) {
      _authToken = await authenticate();
    }
    
    String url = 'https://www.universal-tutorial.com/api/states/$country';
    var res = await getRequest(url, headers: _getHeader());
    return (res as List).map<String>((item) => item['state_name']).toList();
  }
  
  Future<List<String>> getCities(String state) async {
   if(!(await Global().isInternetAvailable())) {
      Global().showToastMessage('Please connect to internet');
      return [];
    }
    if(_authToken == null) {
      _authToken = await authenticate();
    }
    
    String url = 'https://www.universal-tutorial.com/api/cities/$state';
    var res = await getRequest(url, headers: _getHeader());
    return (res as List).map<String>((item) => item['city_name']).toList();
  } 
  
  Future<List<Address>> retrieveStreetSuggestions(String prefix, String city) async {
    if(!(await Global().isInternetAvailable())) {
      return null;
    }
    var url = 'https://us-autocomplete.api.smartystreets.com/suggest?auth-id=4823262c-6f3d-8bae-0834-6270b1cab0ce&auth-token=79d1u7zMJg5b6OFhxGLo&prefix=$prefix&include_only_cities=$city';
    var res = await getRequest(url, headers: {"Accept": "application/json"});
    if(res['suggestions'] == null) {
      return null;
    }
    return (res['suggestions'] as List<dynamic>).map<Address>((jsonMap) => Address.fromPrefsJson(jsonMap as Map)).toList();
  }
}