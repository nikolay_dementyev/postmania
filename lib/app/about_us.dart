import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text('About Us', style: TextStyle(fontFamily: 'Nexabold')),
        leading: InkWell(onTap: () {
          Navigator.pop(context);
        }, child: Icon(Icons.arrow_back_ios)),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
                        style: TextStyle(fontSize: 16),
                      ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            color: Color(0xFFDDDDDD),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                  },
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}