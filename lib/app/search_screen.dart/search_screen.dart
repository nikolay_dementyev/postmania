import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/ui/post_detail.dart';
import 'package:firebase_uploading/app/post/ui/post_item.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/providers/comment_provider.dart';
import 'package:firebase_uploading/providers/search_provider.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SearchScreen extends StatefulWidget {
  
  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<SearchScreen> {

  @override
  void dispose() {
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: ChangeNotifierProvider<SearchProvider>(
          create: (context) {
            return SearchProvider(
              authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
              loadBloc: BlocProvider.of<LoadBloc>(context)
            ); 
          },
          child: PostListPage()
        )
      ),
    );
  }
}

class PostListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    SearchProvider searchProvider = Provider.of<SearchProvider>(context);
    
    return Scaffold(
      backgroundColor: Colors.white,
      body: PostListScreen(searchProvider),
    );
  }
}

class PostListScreen extends BaseListScreen {

  final SearchProvider searchProvider;
  
  PostListScreen(this.searchProvider);
  
  @override
  int getCount() {
    return searchProvider.postList?.length ?? 0;
  }
  
  @override
  Widget buildListItem(BuildContext context, int index) {
    return PostItem(post: searchProvider.postList[index]);
  }
  
  @override
  void onSelected(BuildContext context, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PostDetail(Provider.of<CommentProvider>(context), post: searchProvider.postList[index])));
  }
  
  @override
  void onSearch(String query, BuildContext context) {
    searchProvider.fetchPosts(query);
  }
}