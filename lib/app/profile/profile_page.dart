import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:firebase_uploading/app/profile/profile_edit.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/providers/profile_provider.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/material.dart';
import 'my_post_list.dart';

class ProfilePage extends StatefulWidget {

  final ProfileProvider profileProvider;

  const ProfilePage({Key key, this.profileProvider}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  
  @override
  void initState() {
    super.initState();
    
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Profile'),
        centerTitle: true,
      ),
      body: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        condition: ((prev, state) {
          return state is AuthenticationAuthenticated;
        }),
        builder: (context, state) {
          print(state);
          return Column(
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      const SizedBox(height: 10),
                      Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: state.profile?.avatarUrl == null ? Image.asset('assets/images/man.png', width: 130, height: 130,) : AsyncNetworkImage(photoKey: state.profile.avatarUrl, imgUrl: state.profile.avatarUrl, width: 130, height: 130, boxfit: BoxFit.cover, placeholder: 'assets/images/man.png',),
                          ),
                          const SizedBox(width: 15),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(state.profile?.userName ?? '', style: TextStyle(fontSize: 16, fontFamily: 'Nexabold')),
                              const SizedBox(height: 10),
                              Text(state.profile?.email ?? '', style: TextStyle(fontSize: 16, fontFamily: 'Nexalight')),
                              const SizedBox(height: 10),
                              Text('${state.profile?.cityName ?? 'Utah'}', style: TextStyle(fontSize: 16, fontFamily: 'Nexalight')),
                              const SizedBox(height: 10),
                              Text('${state.profile?.countryName ?? 'United States'}', style: TextStyle(fontSize: 16, fontFamily: 'Nexalight')),
                              const SizedBox(height: 10),
                              InkWell(onTap: () {
                                Navigator.push(context, MaterialPageRoute(builder: (context1) => ProfileEdit(profile: state.profile)));
                              }, child: Text('Edit', style: TextStyle(fontSize: 16, fontFamily: 'Nexabold', color: Colors.red))),
                            ],
                          )
                        ],
                      ),
                      Column( 
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          ExPansionPanel(
                            title: 'About Me',
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Align(
                                alignment: Alignment.bottomLeft,
                                child: Text(
                                  state.profile?.aboutMe != null ? (state.profile.aboutMe.isEmpty ? 'I am a ...' : state.profile.aboutMe) : 'I am a ...',
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: Color(0xFFDDDDDD),
                              border: Border.fromBorderSide(BorderSide(
                                color: Colors.white,
                                width: 1.0
                              ))
                            ),
                            padding: EdgeInsets.symmetric(vertical: 3),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(children: <Widget>[
                                Text('My Posts',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontFamily: 'Nexabold')),
                                Spacer(),
                                // Icon(expaned ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
                              ]),
                            ),
                          ),
                          MyPostList()
                          // ExPansionPanel(
                          //   isExpanded: false,
                          //   title: 'My Posts',
                          //   child: 
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                color: Color(0xFFDDDDDD),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FloatingActionButton(
                      child: Icon(Icons.add),
                      onPressed: () {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                      },
                    ),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}