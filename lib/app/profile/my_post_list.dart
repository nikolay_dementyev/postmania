import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/ui/post_detail.dart';
import 'package:firebase_uploading/app/post/ui/post_edit.dart';
import 'package:firebase_uploading/app/post/ui/post_item.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/dialogs/confirm_dialog_view.dart';
import 'package:firebase_uploading/providers/comment_provider.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/utils/firebase/firestore.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyPostList extends StatefulWidget {
  @override
  _MyPostListState createState() => _MyPostListState();
}

class _MyPostListState extends State<MyPostList> {

  List<Post> _myPosts;
  
  bool isLoading = true;
  
  _loadMyPosts() {
    String userId = BlocProvider.of<AuthenticationBloc>(context).state.profile.userId;
    FireStore.getMyPosts(userId).then((posts) {
      setState(() {
        isLoading = false;
      });
      if(posts == null || posts.isEmpty) {
        return;
      }
      setState(() {
        _myPosts = posts;
      });
    }).catchError((error) {
      setState(() {
        isLoading = false;
      });
    });
  }
  
  _notifyDelete(Post post) {
    BlocProvider.of<PostBloc>(context).add(DeletePost(post));
  }
  
  _deletePost(Post post) async {
     showDialog(context: context, child: ConfirmDialogView(
        description: 'Are you sure you want to delte post ?',
        leftButtonText: 'Cancel',
        rightButtonText: 'OK',
        onAgreeTap: () {
          FireStore.deletePost(post).then((res) {
            if(res) {
              setState(() {
                _myPosts.remove(post);
              });
              _notifyDelete(post);
            }
          });
        },
      )
    ); 
  }
  
  _editPost(Post post) async {
    Post editedPost = await Navigator.push(context, MaterialPageRoute(builder: (context) => PostEditScreen(post)));
    if(editedPost != null) {
      int index = _myPosts.indexWhere((item) => item.id == editedPost.id);
      setState(() {
        _myPosts[index] = editedPost;
      });
    }
  }
  
  @override
  void initState() {
    super.initState();

    _loadMyPosts();
  }
  
  @override
  Widget build(BuildContext context) {

    if(isLoading) return Center(child: LoadingBar());
    
    return _myPosts == null || _myPosts.isEmpty  
    ? Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text('Please add post, there is no post created by you', style: TextStyle(fontSize: 16, color: Colors.red)),
    ) 
    : ListView.builder(
      itemCount: _myPosts?.length ?? 0,
      itemBuilder: (context, int index) {
        return InkWell(onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => PostDetail(Provider.of<CommentProvider>(context), post: _myPosts[index])));
        }, child: PostItem(post: _myPosts[index], isEdit: true, onEdit: () {
          _editPost(_myPosts[index]);
        }, onDelete: () {
         _deletePost(_myPosts[index]);
        }));
      },
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
    );
  }
}