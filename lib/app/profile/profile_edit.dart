import 'dart:io';
import 'package:firebase_uploading/app/location_pick/location_pick.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/dialogs/result_dialog.dart';
import 'package:firebase_uploading/helpers/index.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class InputFiled extends StatelessWidget {
  
  final String hint;
  final String initialValue;
  final String label;
  final ValueChanged<String> onChanged;
  
  const InputFiled({Key key, this.hint, this.onChanged, this.initialValue, this.label}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Container(  
            child: Align(alignment: Alignment.centerLeft,child: Text(label, style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 16)))
          ),
        ),
        TextFormField(
          initialValue: initialValue,
          decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black)
            ),
            hintText: hint
          ),
          onChanged: onChanged,
          style: TextStyle(fontFamily: 'Nexabold'),
        ),
      ],
    );
  }
}

class ProfileEdit extends StatefulWidget {
  
  final Profile profile;
  
  ProfileEdit({Key key, this.profile}) : super(key: key);
  
  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  
  File _avatarFile;
  
  Profile profile;
  
  _onImagePicked(ImageSource source) {
    pickImage(source).then((image) {
      if(image == null) return;
      setState(() {
        _avatarFile = image;
      });
    });
  }
  
  _onSubmit() async {
    print(profile);
    BlocProvider.of<SubmitBloc>(context).add(Submit());
    if(profile.countryName.isEmpty) {
      Global().showToastMessage('Please select country');
      return;
    }
    if(profile.cityName.isEmpty) {
      Global().showToastMessage('Please select city');
      return;
    }
    if(_avatarFile != null) {
      String url = await FireStorage.saveAttachFile(_avatarFile);
      profile.avatarUrl = url;
    }
    bool isUpdated = await FireStore.updateUserDetail(profile);
    if(isUpdated) {
      BlocProvider.of<SubmitBloc>(context).add(SubmitSucceeded());
      // ResultDialog(context, message: 'You successfully saved change').show();
      Global().showToastMessage('You successfully saved change');
      BlocProvider.of<AuthenticationBloc>(context).add(ChangeProfile(profile));
      Navigator.pop(context);
    } else {
      ResultDialog(context, message: 'Something Went Wrong', type: MessageType.ERROR,).show();
      BlocProvider.of<SubmitBloc>(context).add(SubmitFailed());
    }
  }
  
  @override
  void initState() {
    super.initState();
    
    profile = widget.profile.copyWith();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Edit Profile'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              const SizedBox(height: 15),
                InkWell(
                  onTap: () {
                    showCupertinoModalPopup(
                        builder: ((context) {
                          return CupertinoActionSheet(
                            title: Text('Choose Image'),
                            message: Text('Please choose image source'),
                            actions: <Widget>[
                              CupertinoActionSheetAction(
                                onPressed: () {
                                  Navigator.pop(context);
                                  _onImagePicked(ImageSource.gallery);
                                },
                                child: Text('Gallery'),
                              ),
                              CupertinoActionSheetAction(
                                onPressed: () {
                                  Navigator.pop(context);
                                  _onImagePicked(ImageSource.camera);
                                },
                                child: Text('Camera'),
                              )
                            ],
                            cancelButton: CupertinoActionSheetAction(
                              child: Text('Cancel'),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          );
                        }), context: context
                      );
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: _avatarFile != null 
                    ? Image.file(_avatarFile, width: 130, height: 130, fit: BoxFit.cover) 
                    : profile.avatarUrl == null ? Image.asset('assets/images/man.png', width: 130, height: 130,) : AsyncNetworkImage(photoKey: profile.avatarUrl, imgUrl: profile.avatarUrl, width: 130, height: 130, boxfit: BoxFit.cover, placeholder: 'assets/images/man.png',),
                  )
                ),
                const SizedBox(height: 15),
                InputFiled(
                  label: 'Username',
                  hint: 'Username',
                  initialValue: profile.userName,
                  onChanged: (value) {
                    profile.userName = value;
                  },
                ),
                const SizedBox(height: 15),
                LocationPick(
                  country: profile.countryName,
                  state: profile.stateName,
                  city: profile.cityName,
                  address: '',
                  onLocationChanged: (country, state, city, address) {
                    profile.countryName = country;
                    profile.stateName = state;
                    profile.cityName = city; 
                  },
                  padding: 0,
                  showAddress: false,
                  showLabels: true,
                ),
                const SizedBox(height: 15),
                TextArea(
                  initialValue: profile.aboutMe == null ? '' : (profile.aboutMe == 'I am a ...' ? '' : profile.aboutMe),
                  label: 'About me',
                  hint: 'I am  ...',
                  maxCount: 300,
                  showTextCount: true,
                  onChanged: (value) {
                    setState(() {
                      profile.aboutMe = value;
                    });
                  },
                ),
                const SizedBox(height: 40),
                BlocBuilder<SubmitBloc, SubmitState>(
                builder: (context, state) {
                  return  InkWell(
                    onTap: state is Submitting ? null : _onSubmit,
                    child: Container(
                      color: Colors.orange,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      child: Center(
                        child: state is Submitting ? LoadingBar() : Text('Save', style: TextStyle(fontSize: 22, fontFamily: 'Nexabold', color: Colors.white)),
                      ),
                    ),
                  );
                },
              ),
                const SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }
}