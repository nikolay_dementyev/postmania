import 'package:firebase_uploading/app/register/bloc/bloc.dart';
import 'package:firebase_uploading/app/register/ui/register_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../blocs/index.dart';

class RegisterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterBloc(
        submitBloc: BlocProvider.of<SubmitBloc>(context),
        authenticationBloc: BlocProvider.of<AuthenticationBloc>(context)
      ),
      child: RegisterScreen(),
    );
  }
}