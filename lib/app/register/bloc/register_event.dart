import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();

  @override
  List<Object> get props => null;
}

class Register extends RegisterEvent {
  final String email;
  final String userName;
  final String password;
  final String passConf;
  
  Register({@required this.email, @required this.userName,  @required this.password, @required this.passConf});
}
