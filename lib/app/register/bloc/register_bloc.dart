import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_uploading/app/register/bloc/register_repository.dart';
import 'package:firebase_uploading/blocs/authentication/authentication_bloc.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/singletons/global.dart';
import '../../../models/index.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  
  final RegisterRepository repository = RegisterRepository();
  
  final AuthenticationBloc authenticationBloc;
  final SubmitBloc submitBloc;
  
  RegisterBloc({@required this.submitBloc, @required this.authenticationBloc});
  
  @override
  RegisterState get initialState => InitialRegisterState();
  
  @override
  Stream<RegisterState> mapEventToState(
    RegisterEvent event,
  ) async* {
    if(event is Register) {
      yield* _mapRegisterToState(event);
    }
  }
  
  Stream<RegisterState> _mapRegisterToState(Register event) async* {
    submitBloc.add(Submit());
    Profile profile = await repository.register(email: event.email, userName: event.userName, password: event.password, passConf: event.passConf);
    
    if(profile == null) {
      submitBloc.add(SubmitFailed());
    } else {
      submitBloc.add(SubmitSucceeded());
      Global().showToastMessage('Successfully logged in');
      await Future.delayed(Duration(milliseconds: 500));
      authenticationBloc.add(LoggedIn(profile: profile));
    }
  }
}