import 'package:firebase_uploading/helpers/index.dart';
import 'package:firebase_uploading/singletons/global.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import '../../../models/index.dart';

class RegisterRepository {
  Future<Profile> register({String email, String userName, String password, String passConf}) async {
    if(userName.isEmpty || userName.split(' ').length == 1) {
      Global().showToastMessage('Please input full name');
      return null;
    }
    if(email.isEmpty) {
      Global().showToastMessage('Please input email');
      return null;
    }
    if(!isValideEmail(email)) {
      Global().showToastMessage('Please input valid email');
      return null;
    }
    if(password.isEmpty) {
      Global().showToastMessage('Please input password');
      return null;
    }
    if(!isStrongPassword(password)) {
      Global().showToastMessage('Password should be of more than 8 length and have 1 number and 1 capital.');
      return null;
    }
    if(password != passConf) {
      Global().showToastMessage('Password does not match');
      return null;
    }
    Profile profile = Profile(email: email, password: password, userName: userName);
    profile = await FbAuthManager().signUp(profile);
    return profile;
  }
}