import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';

class CityAutoComplete extends TypeAhead {
  
  final List<String> cities;
  final ValueChanged<String> onSubmit;
  final TextEditingController controller;
  
  CityAutoComplete(this.controller, this.cities, {this.onSubmit}) : super(controller: controller, onSubmit: onSubmit);
  
  @override
  Future<List<String>> getSuggesstions(String pattern) async {
    List<String> filteredList = List.from(cities);
    if(cities.isEmpty) return [];
    
    filteredList.retainWhere((item) => item.toLowerCase().startsWith(pattern.toLowerCase()));
    return filteredList;
  }
}