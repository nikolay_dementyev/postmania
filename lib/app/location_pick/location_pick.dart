import 'package:firebase_uploading/app/common/address_input.dart';
import 'package:firebase_uploading/app/location_pick/city_auto_complete.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/models/profile.dart';
import 'package:firebase_uploading/singletons/global.dart';
import 'package:firebase_uploading/widgets/custom_picker.dart';
import 'package:firebase_uploading/widgets/diable.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

typedef LocationChangedCallback(String country, String state, String city, String address);

class LocationPick extends StatefulWidget {
  
  final double padding;
  final bool showLabels;
  final bool showAddress;
  
  final String country;
  final String state;
  final String city;
  final String address;
  
  final LocationChangedCallback onLocationChanged;

  const LocationPick({Key key, @required this.onLocationChanged, this.showLabels = false, this.showAddress = true, this.padding = 10, this.country, this.state, this.city, this.address}) : super(key: key);
  
  @override
  _LocationPickState createState() => _LocationPickState();
}

class _LocationPickState extends State<LocationPick> {

  PostRepository postRepository;
  
  CustomPickerController _countryController;
  CustomPickerController _stateController;

  TextEditingController _cityController;
  TextEditingController _addressController;
  
  String _currentCountry = '';
  String _currentRegion = '';
  String _currentCity = '';
  String _currentAddress = '';
  
  List<String> _countries = [];
  List<String> _states = [];
  List<String> _cities = [];
  
  @override
  void initState() {
    super.initState();
    
    postRepository = PostRepository();
    
    _cityController = TextEditingController();
    _addressController = TextEditingController();
    _fetchData();
  }
  
  _onLocatinChanged() {
    widget.onLocationChanged(_currentCountry, _currentRegion, _currentCity, _currentAddress);
  }
  
  _fetchData() async {
    
    if(widget.country != null) {
      _currentCountry = widget.country;
      _currentRegion = widget.state ?? 'Califonia';
      _currentCity = widget.city;
      _currentAddress = widget.address;
    } else {
      bool isPermitted = await Global().checkPermission(PermissionGroup.location);
      
      if(isPermitted) {
        Position position = await Geolocator().getCurrentPosition();
        var addresses = await Geocoder.local.findAddressesFromCoordinates(Coordinates(position.latitude, position.longitude));
        var address = addresses.first;
        _currentCountry = address.countryName;
        _currentRegion = address.adminArea;
        _currentCity = address.locality;
        _currentAddress = address.addressLine;
      }
    }

    _addressController.text = _currentAddress;
    _cityController.text = _currentCity;
    
    _onLocatinChanged();
    
    _fetchCountries();
  }
  
  _fetchCountries() async {
    _countries = await postRepository.getCoutries();
    if(_countries.contains(_currentCountry)) {
      _countries.remove(_currentCountry);
      _countries.insert(0, _currentCountry);
    } else {
      
    }
    _countryController.setSuggestions(_countries);
  }
  
  _fetchStates() async {
    setState(() {
      _cities = [];
    });
    _stateController.setSuggestions([]);
    _states = await postRepository.getRegions(_currentCountry);
    if(_states.contains(_currentRegion)) {
      _states.remove(_currentRegion);
      _states.insert(0, _currentRegion);
    }
    _stateController.setSuggestions(_states);
  }
  
  _fetchCities() async {
    setState(() {
      _cities = [];
    });
    _cities = await postRepository.getCities(_currentRegion);
    if(_cities.contains(_currentCity)) {
      _cities.remove(_currentCity);
      _cities.insert(0, _currentCity);
    }
    setState(() {
      
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.padding),
      child: Column(
        children: <Widget>[
          widget.showLabels 
          ? Padding(
            padding: EdgeInsets.only(bottom: 10),
            child: Container(  
              child: Align(alignment: Alignment.centerLeft,child: Text('Country', style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 16)))
            ),
          )
          : Container(),
          CustomPicker(
            onPickerCreated: (controller) {
              _countryController = controller;
            },
            suggestions: [],
            hint: 'Select Country',
            onSubmit: (value) {
              // if(value == _currentCountry) return;
              _currentCountry = value;
              _onLocatinChanged();
              _fetchStates();
            },
          ),
          const SizedBox(height: 15),
          widget.showLabels 
          ? Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(  
              child: Align(alignment: Alignment.centerLeft,child: Text('State', style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 16)))
            ),
          )
          : Container(),
          CustomPicker(
            onPickerCreated: (controller) {
              _stateController = controller;
            },
            suggestions: [],
            hint: 'Select State',
            onSubmit: (value) {
              // if(_currentRegion == value) return;
              _currentRegion = value;
              _onLocatinChanged();
              _fetchCities();
            },
          ),
          const SizedBox(height: 15),
          widget.showLabels 
          ? Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(  
              child: Align(alignment: Alignment.centerLeft,child: Text('City', style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 16)))
            ),
          )
          : Container(),
          Disable(isDisabled: _cities.isEmpty, child: CityAutoComplete(_cityController, _cities, onSubmit: (city) {
            setState(() {
              _currentCity = city;
            });
            _onLocatinChanged();
          },)),
          widget.showAddress 
          ? Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15),
            child: AddressInputWidget(
              city: _currentCity,
              controller: _addressController,
              onSubmit: (value) {
                _currentAddress = value;
                _onLocatinChanged();
              },
              hint: 'Enter Address',
              isAutoAddress: true,
              onLocationClicked: () {
                
              },
            ),
          )
          : Container(),
        ],
      ),
    );
  }
}