import 'package:firebase_uploading/app/about_us.dart';
import 'package:firebase_uploading/app/contact_us.dart';
import 'package:firebase_uploading/app/cookie_policy.dart';
import 'package:firebase_uploading/app/copyright.dart';
import 'package:firebase_uploading/app/how_to_use.dart';
import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:firebase_uploading/app/privacy_policy/privacy_policy_screen.dart';
import 'package:firebase_uploading/app/profile/profile_page.dart';
import 'package:firebase_uploading/dialogs/confirm_dialog_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_uploading/blocs/authentication/bloc.dart';
import 'package:share/share.dart';
import '../../terms_of_use.dart';

TextStyle menuItemStyle = TextStyle(color: Colors.grey, fontSize: 18);

class NavItem extends StatelessWidget {

  final VoidCallback onTap;
  final String title;
  final int delay;
  final IconData icon;
  const NavItem({Key key, this.onTap, this.title, this.delay = 300, @required this.icon}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Row(
              children: <Widget>[
                const SizedBox(width: 10),
                Icon(icon, size: 30, color: Colors.orange),
                const SizedBox(width: 15),
                Text(title, style: TextStyle(color: Colors.black, fontSize: 16, height: 1),),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class NavMenuWidget extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
          Container(
              width: 300,
              height: 200,
              color: Colors.orange,
              child: Center(
                  child: Text('Postmania', style: TextStyle(fontFamily: 'Nexabold', color: Colors.white, fontSize: 25),)
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: 5, right: 45),
              child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                SizedBox(height: 33),
                NavItem(
                  icon: Icons.person,
                  delay: 300,
                  title: 'My Profile',                  
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage()));
                  },
                ),
                NavItem(
                  icon: Icons.add,
                  delay: 300,
                  title: 'Add Post',                  
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                  },
                ),
                NavItem(
                  icon: Icons.description,
                  delay: 400,
                  title: 'Terms of Use',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TermsOfUse()));
                  },
                ),
                NavItem(
                  icon: Icons.description,
                  delay: 400,
                  title: 'Privacy Policy',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PrivacyPolicy()));
                  },
                ),
                NavItem(
                  icon: Icons.description,
                  delay: 400,
                  title: 'Copyright',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CopyRight()));
                  },
                ),
                NavItem(
                  icon: Icons.description,
                  delay: 400,
                  title: 'Cookies Policy',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => CookiePolicy()));
                  },
                ),
                 NavItem(
                  icon: Icons.phone,
                  delay: 400,
                  title: 'Contact Us',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => ContactUs()));
                  },
                ),
                NavItem(
                  icon: Icons.group,
                  delay: 400,
                  title: 'About Us',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AboutUs()));
                  },
                ),
                NavItem(
                  icon: Icons.question_answer,
                  delay: 400,
                  title: 'How to Use',
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => HowToUse()));
                  },
                ),
                NavItem(
                  icon: Icons.share,
                  delay: 400,
                  title: 'Share',
                  onTap: () {
                    Navigator.pop(context);
                    Share.share('https://drive.google.com/open?id=1ff00tycM9pS2AN3_aoWCu-KoYgJ8xbTS');
                  },
                ),
                NavItem(
                  icon: Icons.power_settings_new,
                  delay: 500,
                  title: 'Log out',
                  onTap: () {
                    Navigator.pop(context);
                    BuildContext appContext = context;
                    BlocProvider.of<AuthenticationBloc>(appContext).add(LoggedOut());
                    // showDialog(context: context, child: ConfirmDialogView(
                    //   title: 'Logout',
                    //   description: 'Are you sure you want to logout ?',
                    //   leftButtonText: 'No',
                    //   rightButtonText: 'Yes',
                    //   onAgreeTap: () {
                    //   },
                    // ));
                  },
                ),
                SizedBox(height: 50)
              ]),
            ),
          ],
        ),
      ),
    );
  }
}