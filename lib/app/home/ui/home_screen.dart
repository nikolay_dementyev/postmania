import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:firebase_uploading/app/post/ui/post_list.dart';
import 'package:flutter/material.dart';
import 'package:firebase_uploading/app/home/ui/side_menu.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  bool isShowSearchBar = false;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Postmania'),
        centerTitle: true,
        actions: <Widget>[
          InkWell(onTap: () {
            setState(() {
              isShowSearchBar = !isShowSearchBar;
            });
          }, child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          ))
        ],
      ),
      drawer: SizedBox(
        width: 300,
        child: Drawer(
          elevation: 5,
          child: NavMenuWidget()),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: PostList(isShowSearchBar: isShowSearchBar),
          ),
          Container(
            color: Color(0xFFDDDDDD),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                  },
                ),
              ),
            ),
          )
        ]
      ),
    );
  }
}