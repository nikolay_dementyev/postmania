import 'package:flutter/material.dart';
import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/home/ui/home_screen.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => HomeBloc(),
        child: HomeScreen(),
      ),
    );
  }
}