import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/material.dart';

class PrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Privacy Policy', style: TextStyle(fontFamily: 'Nexabold')),
        leading: InkWell(onTap: () {
          Navigator.pop(context);
        }, child: Icon(Icons.arrow_back_ios)),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                 ExPansionPanel(
                   title: 'Terms Of Use',
                   child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do\n\nDummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
                        style: TextStyle(fontSize: 16),
                      ),
                  )
                 ),
                 ExPansionPanel(
                   title: 'Privacy',
                   child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do\n\nDummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
                        style: TextStyle(fontSize: 16),
                      ),
                  ),
                 ),
                 ExPansionPanel(
                   title: 'Copyright',
                   child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do\n\nDummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
                        style: TextStyle(fontSize: 16),
                      ),
                  ),
                 ),
                 ExPansionPanel(
                   title: 'Cookies Policy',
                   child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                        'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do\n\nDummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
                        style: TextStyle(fontSize: 16),
                      ),
                  ),
                 ),                 
                ],
              ),
            ),
          ),
          BlocProvider.of<AuthenticationBloc>(context).state is AuthenticationAuthenticated 
          ? Container(
            color: Color(0xFFDDDDDD),
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FloatingActionButton(
                  child: Icon(Icons.add),
                  onPressed: () {
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                  },
                ),
              ),
            ),
          )
          : Container()
        ],
      ),
    );
  }
}

// class PrivacyPolicy extends BaseTabBarScreen {
  
//   @override
//   AppBar buildAppBar(BuildContext context) {
//     return AppBar(
//         title: Text('Privacy Policy', style: TextStyle(fontFamily: 'Nexabold')),
//         leading: InkWell(onTap: () {
//           Navigator.pop(context);
//         }, child: Icon(Icons.arrow_back_ios)),
//       );
//   }
  
//   @override
//   List<Widget> getChildren() {
//     return [
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//               'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do\n\nDummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
//               style: TextStyle(fontSize: 16),
//             ),
//         ),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//               'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
//               style: TextStyle(fontSize: 16),
//             ),
//         ),
//         Padding(
//           padding: const EdgeInsets.all(8.0),
//           child: Text(
//               'Dummy Poicy Text for expandable static text and contact us form screens, incididunt ut labore et dolore magna. Ut enim ad ullamco laboris nisi ut aliquip ex ea commodo consequat\nDescription Text sit amet, consetetur adipicing elit, sed do',
//               style: TextStyle(fontSize: 16),
//             ),
//         ),
//     ];
//   }
  
//   @override
//   List<String> getTitles() {
//     return [
//       'Privacy1',
//       'Privacy2',
//       'Privacy3',
//     ];
//   }
// }