import 'package:flutter/material.dart';

class InfoInput extends StatelessWidget {

  final TextEditingController controller;
  final Widget right;
  final IconData icon;
  final String hint;
  final TextInputType inputType;
  final bool obscureText;
  final ValueChanged<String> onChanged;
  
  const InfoInput({Key key, this.right, @required this.icon, @required this.hint, this.onChanged, this.controller, this.inputType = TextInputType.text, this.obscureText = false}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      constraints: BoxConstraints(
        minWidth: 239,
        maxWidth: 350
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5))
      ),
      padding: EdgeInsets.symmetric(vertical: 6, horizontal: 15),
      child: Row(
        children: <Widget>[
          Icon(icon, color: Colors.black54),
          const SizedBox(width: 10),
          Expanded(
            child: TextField(
              keyboardType: inputType,
              obscureText: obscureText,
              controller: controller ?? TextEditingController(),
              decoration: InputDecoration(
                hintText: hint,
                hintStyle: TextStyle(color: Colors.black54, fontSize: 16),
                border: InputBorder.none
              ),
              style: TextStyle(color: Colors.black54, fontSize: 16),
              onChanged: (value) {
                if(onChanged != null) {
                  onChanged(value);
                }
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5),
            child: right ?? Container(),
          )
        ]
      ),
    );
  }
}