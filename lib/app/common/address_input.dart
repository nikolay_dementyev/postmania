import 'package:firebase_uploading/utils/rest/location_client.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';

class AddressInputWidget extends TypeAhead {
  
  final bool isAutoAddress;
  final ValueChanged<String> onSubmit;
  final VoidCallback onLocationClicked;
  final TextEditingController controller;
  final String city;
  AddressInputWidget( {this.city = '', String hint, this.controller, this.onSubmit, this.isAutoAddress = false, this.onLocationClicked, }) : super(hint: hint, onSubmit: onSubmit, isAutoAddress: isAutoAddress, onLocationClicked: onLocationClicked, controller: controller);
  
  @override
  Future<List<String>> getSuggesstions(String pattern) async {
    var suggestions = await LocationApiClient().retrieveStreetSuggestions(pattern, city);
    if(suggestions == null || suggestions.isEmpty) return [];
    return suggestions.map<String>((suggestion) => suggestion.name).toList();
  }
}