import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange,
      body: SafeArea(
        child: Center(
          child: OutlinedText(text: 'Postmania', size: 45, outsideColor: Colors.white, insideColor: Theme.of(context).scaffoldBackgroundColor)
        )
      )
    );
  }
}