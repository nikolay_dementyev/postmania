import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/dialogs/result_dialog.dart';
import 'package:firebase_uploading/dialogs/warning_dialog_view.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/widgets/custom_picker.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/material.dart';

class ContactUs extends StatefulWidget {

  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
 
  String os = '';
  String reason = '';
  String description = '';
  
  _onSubmit() async {
    if(description.isEmpty || description.length < 30) {
      showDialog(context: context, child: WarningDialog(message: 'Please add minimum 30 characters'));
      return;
    }    
    if(os.isEmpty) {
      ResultDialog(context, message: 'Please select operating system', type: MessageType.WARNING,).show();
      return;
    }
    if(reason.isEmpty) {
      ResultDialog(context, message: 'Please select reason', type: MessageType.WARNING,).show();
      return;
    }  
    bool isSent = await Global().sendEmail(os, reason, description);
    // if(isSent) {
    //   ResultDialog(context, message: 'Successfully Submitted').show();
    // } else {
    //   ResultDialog(context, message: 'Failed to send', type: MessageType.ERROR,).show();
    // }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Contact Us', style: TextStyle(fontFamily: 'Nexabold')),
        leading: InkWell(onTap: () {
          Navigator.pop(context);
        }, child: Icon(Icons.arrow_back_ios)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 8, right: 8, bottom: 5),
              child: Text(
                  'Please fill out the following form for feedback or customer support',
                  style: TextStyle(fontFamily: 'Nexabold', fontSize: 16),
                ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, top: 10),
              child: TextArea(
                showLabel: false,
                hint: 'Description',
                maxCount: 500,
                minHeight: 300,
                onChanged: (value) {
                  description = value;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CustomPicker(
                suggestions: [
                  'Android',
                  'iPhone'
                ],
                intialValue: 'Android',
                hint: 'Select Operating System',
                onSubmit: (value) {
                  os = value;
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CustomPicker(
                suggestions: [
                  'Video player not working',
                  'Comment error'
                ],
                hint: 'Select Reason',
                intialValue: 'Video player not working',
                onSubmit: (value) {
                  reason = value;
                },
              ),
            ),
            const SizedBox(height: 15),
            BlocBuilder<SubmitBloc, SubmitState>(
             builder: (context, state) {
               return  InkWell(
                onTap: state is Submitting ? null : _onSubmit,
                child: Container(
                  // color: Color(0xFF00A500),
                  color: Colors.orange,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Center(
                    child: state is Submitting ? LoadingBar() : Text('Submit', style: TextStyle(fontSize: 22, fontFamily: 'Nexabold', color: Colors.white)),
                  ),
                ),
              );
             },
           ),
           const SizedBox(height: 10)
          ],
        ),
      ),
    );
  }
}