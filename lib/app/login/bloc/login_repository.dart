import 'package:firebase_uploading/utils/firebase/index.dart';

import '../../../models/index.dart';

class LoginRepository {
  
  Future<Profile> logIn(String email, String password) async {
    Profile profile = await FbAuthManager().signIn(email, password);
    if(profile == null) return null;
    profile = await FireStore.getUserDetail(profile.userId);
    profile.email = email;
    if(profile == null) return null;
    return profile;
  }
  
  Future<Profile> googleSignIn() async {
    return FbAuthManager().signInWithGoogle();
  }
}