import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
  
  @override
  List<Object> get props => null;
}

class Login extends LoginEvent {

  final String email;
  final String password;
  
  Login({@required this.email, @required this.password});
}

class SignInWIthGoogle extends LoginEvent {}