import 'package:flutter/material.dart';
import 'package:firebase_uploading/app/login/bloc/bloc.dart';
import 'package:firebase_uploading/app/login/ui/login_screen.dart';
import 'package:firebase_uploading/blocs/authentication/bloc.dart';
import 'package:firebase_uploading/blocs/index.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LoginBloc(authenticationBloc: BlocProvider.of<AuthenticationBloc>(context), submitBloc: BlocProvider.of<SubmitBloc>(context)),
      child: LoginScreen(),
    );
  }
}