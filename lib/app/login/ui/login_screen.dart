import 'package:firebase_uploading/app/common/address_input.dart';
import 'package:firebase_uploading/app/privacy_policy/privacy_policy_screen.dart';
import 'package:firebase_uploading/app/register/ui/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:firebase_uploading/app/login/bloc/bloc.dart';
import 'package:firebase_uploading/blocs/submit/bloc.dart';
import 'package:firebase_uploading/widgets/index.dart';
import '../../common/index.dart';

class LoginScreen extends StatefulWidget {
  
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with SingleTickerProviderStateMixin {

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  
  AnimationController _animationController;
  Animation<double> _animation; 
  
  @override
  void initState() {
    super.initState();
    
    _animationController = AnimationController(vsync: this, duration: Duration(milliseconds: 700));
    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _animationController,
        curve: Interval(0.5, 1.0, curve: Curves.fastOutSlowIn)
      )
    );
    _animationController.forward();
  }
  
  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.orange,
      body: AnimatedBuilder(
        animation: _animationController,
        builder: (context, child) {
          return FadeTransition(
            opacity: _animation,
            child: Transform(
              transform: Matrix4.translationValues(0, 100 * (1.0 - _animation.value), 0),
              child: SafeArea(
                child: SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      const SizedBox(height: 60),
                      Text('Please Login to Postmania', style: TextStyle(fontSize: 22, fontFamily: 'Nexabold', fontWeight: FontWeight.bold, color: Colors.white)),
                      const SizedBox(height: 50),

                      // AddressInputWidget(),


                      Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            InfoInput(controller: _emailController, icon: Icons.person, inputType: TextInputType.emailAddress, hint: 'Email'),
                            const SizedBox(height: 15),
                            InfoInput(controller: _passwordController, icon: Icons.lock, hint: 'Password', obscureText: true),
                            const SizedBox(height: 40),
                            BlocBuilder<SubmitBloc, SubmitState>(
                              builder: (context, state) {
                                return SubmitButton(title: 'Login', onPressed: () {
                                  BlocProvider.of<LoginBloc>(context).add(Login(email: _emailController.text, password: _passwordController.text));
                                });
                              },
                            ),
                            const SizedBox(height: 15),
                            GoogleSignButton(onPressed: () {
                              BlocProvider.of<LoginBloc>(context).add(SignInWIthGoogle());
                            }),
                            const SizedBox(height: 40),
                            IntrinsicWidth(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text('Don\'t you have account ? ', style: TextStyle(fontSize: 16, color: Colors.white)),
                                  InkWell(
                                    onTap: () {
                                      _animationController.reverse().then(
                                      (void data) {
                                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RegisterPage()));
                                      }
                                      );
                                    },
                                    child: IntrinsicWidth(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Text('Register', style: TextStyle(fontSize: 16, fontFamily: 'Nexabold', color: Colors.blue, fontWeight: FontWeight.w600)),
                                          const SizedBox(height: 1),
                                          Container(
                                            color: Colors.white,
                                            height: 1,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            const SizedBox(height: 20),
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text('By continuing, you accept our ', style: TextStyle(fontSize: 16, color: Colors.white)),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(context, MaterialPageRoute(builder: (context) => PrivacyPolicy()));
                                  },
                                  child: IntrinsicWidth(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text('Policies',  style: TextStyle(fontSize: 16, fontFamily: 'Nexabold', color: Colors.blue)),
                                        const SizedBox(height: 1),
                                        Container(
                                          color: Colors.white,
                                          height: 1,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 40),
                    ]
                  ),
                )
              ),
            ),
          );
        },
      ),
    );
  }
}