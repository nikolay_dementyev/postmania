import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/singletons/global.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import 'package:firebase_uploading/utils/rest/location_client.dart';
import 'package:firebase_uploading/utils/sqflite/location_db_client.dart';

class PostRepository {
  
  Future<List<Post>> fetchPosts(String query) async {
    return await FireStore.fetchPosts(query);
  }
  
  Future<Post> addPost({PostType type, String posterId, String posterName, String title, String country, String state, String city, String address, File thumbFile, File contentFile}) async {
    
    if(title.isEmpty) {
      Global().showToastMessage('Please input description');
      return null;
    }
    if(country == null) {
      Global().showToastMessage('Please select country');
      return null;
    }
    if(state == null) {
      Global().showToastMessage('Please select state');
      return null;
    }
    if(city == null) {
      Global().showToastMessage('Please select city');
      return null;
    }
    if(address == null) {
      Global().showToastMessage('Please input address');
      return null;
    }
    
    String thumbUrl;
    String contentUrl;
    if(contentFile == null && thumbFile == null) {
      thumbUrl = '';
      contentUrl = '';
    } else {
      contentUrl = await FireStorage.saveAttachFile(contentFile);
      if(type == PostType.Video) {
        thumbUrl = await FireStorage.saveAttachFile(thumbFile); 
      } else {
        thumbUrl = contentUrl;
      }
    }
    
    Post post = await FireStore.addPost(postType: type, posterId: posterId, posterName: posterName, title: title, country: country, state: state, city: city, address: address, thumbUrl: thumbUrl, url: contentUrl);
    post.timeStamp = Timestamp.now();
    if(post != null) {
      Global().showToastMessage('Successfully uploded');
      return post;
    } else {
      return post;
    }
   }
   
   Future updatePost(Post post) async {
     await FireStore.updatePost(post);
   }
   
   Future<List<String>> getCoutries() async {     
    List<String> countries = await LocationDBClient().getCountries();
    if(countries.isNotEmpty) {
      return countries;
    }
     /// If there are no data in SQLite, then fetch country list from http://restcountries.net RestApi
     countries = await LocationApiClient().getCountries();
     for(int i = 0 ; i < countries.length; i++) {
      LocationDBClient().saveCountry(countries[i]);
     }
     return countries;
   }
   
   Future<List<String>> getRegions(String country) async {
    
     /// First Fetch Data From Local Database
     List<String> regions = await LocationDBClient().getStates(country);
     if(regions.isNotEmpty) {
       return regions;
     }
     
     /// If not exists in sqlite database, fetch data from network
     regions = await LocationApiClient().getRegions(country);
     
     /// Then save to local database
     int countryId = await LocationDBClient().getCountryIdByName(country);
     if(countryId != -1) {
      for(int i = 0; i < regions.length; i++) {
        LocationDBClient().saveState(countryId, regions[i]);
      }
     }
     return regions;
   }
   
   Future<List<String>> getCities(String state) async {
     /// First Fetch Data From Local Database
     List<String> cities = await LocationDBClient().getCities(state);
     if(cities.isNotEmpty) {
       return cities;
     }
     
     /// Then Fetch Data From Internet
      cities = await LocationApiClient().getCities(state);
      print('${cities.length} cities was saved to db');
      int stateId = await LocationDBClient().getStateIdByName(state);
      if(stateId != -1) {
        for(int i = 0; i < cities.length; i++) {
          LocationDBClient().saveCity(stateId, cities[i]);
        }
      }
      return cities;
   }
}