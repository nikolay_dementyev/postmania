import 'package:equatable/equatable.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';

abstract class PostState extends Equatable {
  
  const PostState();
  
  @override
  List<Object> get props => null;
}

class InitialPostState extends PostState {
  @override
  List<Object> get props => [];
}

class PostListChanged extends PostState {
  final List<Post> postList;
  PostListChanged(this.postList);
}
