import 'package:cloud_firestore/cloud_firestore.dart';

enum PostType {
  Video,
  Image
}

class Post {
  
  String id;
  PostType postType;
  String posterId;
  String posterName;
  String description;
  String country;
  String region;
  String city;
  String address;
  String thumbnailUrl;
  String postUrl;
  bool me;
  Timestamp timeStamp;
  
  String createdTime;
  
  // List<Comment> comments;
  List<String> likedUsers;
  List<String> dislikedUsers;
  

  // For Sqlite
  int dbId;
  String contentFilePath;
  String thumbFilePath;
  
  Post() {
    me = false;
  }
  
  Post.fromDbJson(Map jsonMap) {
    dbId = jsonMap['id'];
    postType = jsonMap['type'] == 'image' ? PostType.Image : PostType.Video;
    description = jsonMap['title'];
    country = jsonMap['country'];
    region = jsonMap['state'];
    city = jsonMap['city'];
    address = jsonMap['address'];
    posterName = jsonMap['posterName'];
    posterId = jsonMap['posterId'];
    contentFilePath = jsonMap['contentFilePath'];
    thumbFilePath = jsonMap['thumbFilePath'];
  }
  
  Post.fromJson(Map jsonMap,) {
    me = false;
    id = jsonMap['id'];
    postType = jsonMap['type'] == 'image' ? PostType.Image : PostType.Video;
    description = jsonMap['description'];
    country = jsonMap['country'];
    region = jsonMap['state'];
    city = jsonMap['city'];
    address = jsonMap['address'];
    thumbnailUrl = jsonMap['thumbUrl'];
    postUrl = jsonMap['url'];
    if(jsonMap['likedUsers'] != null) {
      likedUsers = (jsonMap['likedUsers'] as List).map<String>((item) => item).toList();
    }
    if(jsonMap['dislikedUsers'] != null) {
      dislikedUsers = (jsonMap['dislikedUsers'] as List).map<String>((item) => item).toList();
    }
    posterName = jsonMap['poster_name'];
    posterId = jsonMap['poster_id'];
    timeStamp = jsonMap['timestamp'];
  }

  Map<String, dynamic> toMap( {bool isUpdate = false}) {
    Map<String, dynamic> jsonMap = {
      "id" : id,
      "description": description,
      "type" : postType == PostType.Image ? 'image' : 'video',
      'country' : country,
      'state' : region,
      'city' : city,
      'address' : address,
      'url' : postUrl,
      'poster_name': posterName,
      'poster_id' : posterId,
      'thumbUrl' : thumbnailUrl,
      'likedUsers': likedUsers ?? [],
      'dislikedUsers': dislikedUsers ?? [],
      'timestamp': FieldValue.serverTimestamp()
    };
    if(isUpdate) {
      jsonMap.remove('timestamp');
    }
    return jsonMap;
  }
}