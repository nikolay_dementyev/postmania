import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:connectivity/connectivity.dart';
import 'package:firebase_uploading/app/post/ui/post_list.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/blocs/submit/bloc.dart';
import 'package:firebase_uploading/singletons/global.dart';
import 'package:firebase_uploading/utils/firebase/firestore.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import 'package:firebase_uploading/utils/sqflite/post_db_client.dart';
import './bloc.dart';
import 'package:meta/meta.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  
  final NavigatorBloc navigatorBloc;
  final AuthenticationBloc authenticationBloc;
  final LoadBloc loadBloc;
  final SubmitBloc submitBloc;
  final PostRepository repository = PostRepository();
  
  StreamSubscription _postAddListener;
  Connectivity _connectivity;
  
  bool isNetworkAvailable = true;
  
  List<Post> _postList;
  
  List<String> countries;
  List<String> stateList = [];
  List<String> cityList = [];
  List<String> addressList = [];
  
  PostBloc({@required this.submitBloc, @required this.authenticationBloc, @required this.navigatorBloc, @required this.loadBloc});
  
  @override
  PostState get initialState => InitialPostState();
  
  @override
  Future<void> close() {
    _postAddListener.cancel();
    
    return super.close();
  }
  
  @override
  Stream<PostState> mapEventToState(
    PostEvent event,
  ) async* {
    if(event is Initialize) {
      yield* _initialize();
    } else if(event is AddPost) {
      yield* _mapAddToState(event);
    } else if(event is FetchPosts) {
      yield* _mapFetchPostsToState(event.query);
    } else if(event is RecieveNewPost) {
      yield* _mapRecieveToState(event);
    } else if(event is UpdatePost) {
      yield* _mapUpdateToState(event);
    } else if(event is AddBufferedPosts) {
      yield* _addBufferedPosts();
    } else if(event is DeletePost) {
      _postList.removeWhere((item) => item.id == event.post.id);
      if(state is PostListChanged) {
        yield InitialPostState();
      }
      yield PostListChanged(_postList);
    } else if(event is EditPost) {
      int index = _postList.indexWhere((item) => item.id == event.post.id);
      if(index != -1 && index != null) {
        _postList[index] = event.post;
        if(state is PostListChanged) {
          yield InitialPostState();
        }
        yield PostListChanged(_postList);
      }
    }
  }
  
  Stream<PostState> _initialize() async* {
    
    _connectivity = Connectivity();
    isNetworkAvailable = await Global().isInternetAvailable();
    _connectivity.onConnectivityChanged.listen((ConnectivityResult result) {
      if(result == ConnectivityResult.none) {
        if(!isNetworkAvailable) return;
        
        isNetworkAvailable = false;
        print('Network is not available');
      } else {
        if(isNetworkAvailable) return;
        isNetworkAvailable = true;
        add(AddBufferedPosts());
        print('Network is available');
      }
    });
  }
  
  Stream<PostState> _addBufferedPosts() async* {
    if(!isNetworkAvailable) return;
    
    List<Post> bufferedPosts = await PostDBClient().fetchPosts();
    if(bufferedPosts.isEmpty) return;
    
    for(int i = 0; i < bufferedPosts.length; i++) {
      File contentFile = File(bufferedPosts[i].contentFilePath);
      File thumbFile = File(bufferedPosts[i].thumbFilePath);
      Post post = await repository.addPost(
        type: bufferedPosts[i].postType, 
        posterId: bufferedPosts[i].posterId, 
        posterName: bufferedPosts[i].posterName, 
        title: bufferedPosts[i].description, 
        country: bufferedPosts[i].country, 
        state: bufferedPosts[i].region, 
        city: bufferedPosts[i].city, 
        address: bufferedPosts[i].address, 
        contentFile: contentFile, 
        thumbFile: thumbFile
      );
      
      if(post == null) {
        return;
      }
      post.me = true;
      yield InitialPostState();
      if(_postList == null) {
        _postList = [];
      }
      post.createdTime = '1 min ago';
      _postList.insert(0, post);
      yield PostListChanged(_postList);
      await PostDBClient().deletePost(bufferedPosts[i].dbId);
    }
  }
  
  Stream<PostState> _mapUpdateToState(UpdatePost event) async* {
    repository.updatePost(event.post);
  }
  
  Stream<PostState> _mapFetchPostsToState(String query) async* {
    loadBloc.add(Load());
    _postList = await repository.fetchPosts(query);
    if(_postList != null) {
      String userId = authenticationBloc.state.profile.userId;
      for(int i = 0; i < _postList.length; i++) {
        Post post = _postList[i];
        if(post.posterId == userId) {
          post.me = true;
        } 
      }
      loadBloc.add(LoadSucceeded());
      if(state is PostListChanged) {
        yield InitialPostState();
      }
      yield PostListChanged(_postList);
    } else {
      loadBloc.add(LoadFailed());
    }
    yield* _addBufferedPosts();
    _postAddListener = FireStore.addPostChangeListener(onPostAdded: (post) {
      if(_postList == null || _postList.isEmpty) {
        add(RecieveNewPost(post));
      } else {
        Post existingPost = _postList.firstWhere((item) => item.id == post.id, orElse: null);
        if(existingPost == null) {
          add(RecieveNewPost(post));
        } else {
          
        }
      }
    });
  }
  
  Stream<PostState> _mapRecieveToState(RecieveNewPost event) async* {
    String myId = authenticationBloc.state.profile.userId;
    if(event.post.posterId == myId) {
      return;
    }
    yield InitialPostState();
    if(_postList == null) {
      _postList = [];
    }
    _postList.insert(0, event.post);
    yield PostListChanged(_postList);
  } 
  
  Stream<PostState> _mapAddToState(AddPost event) async* {
    
    submitBloc.add(Submit());
    String posterName = authenticationBloc.state.profile.userName;
    String posterId = authenticationBloc.state.profile.userId;
    
    if(!isNetworkAvailable) {
      await PostDBClient().addPost(
        type: event.postType, 
        posterId: posterId, 
        posterName: posterName, 
        title: event.descripton, 
        country: event.country, 
        state: event.state, 
        city: event.city, 
        address: event.address, 
        contentFile: event.contentFile, 
        thumbFile: event.thumbFile
      );
      submitBloc.add(SubmitFailed());
      navigatorBloc.add(GoBack());
      return;
    }
    
    Post post = await repository.addPost(
      type: event.postType, 
      posterId: posterId, 
      posterName: posterName, 
      title: event.descripton, 
      country: event.country, 
      state: event.state, 
      city: event.city, 
      address: event.address, 
      contentFile: event.contentFile, 
      thumbFile: event.thumbFile
    );
    if(post == null) {
      submitBloc.add(SubmitFailed());
      return;
    }
    post.me = true;
    if(post != null) {
      submitBloc.add(SubmitSucceeded());
      yield InitialPostState();
      if(_postList == null) {
        _postList = [];
      }
      post.createdTime = '1 min ago';
      _postList.insert(0, post);
      yield PostListChanged(_postList);
      await Future.delayed(Duration(milliseconds: 300));
      navigatorBloc.add(GoBack());
    } else {
      submitBloc.add(SubmitFailed());
    }
  }
}