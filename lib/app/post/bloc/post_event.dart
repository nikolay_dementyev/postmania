import 'dart:io';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'post_model.dart';

abstract class PostEvent extends Equatable {
  const PostEvent();
  
  @override
  List<Object> get props => null;
}

class Initialize extends PostEvent {}

class FetchPosts extends PostEvent {
  final String query;
  
  FetchPosts({this.query = ''});
}

class RecieveNewPost extends PostEvent {
  final Post post;

  RecieveNewPost(this.post);
}

class AddPost extends PostEvent {
  
  final PostType postType;
  final File thumbFile;
  final File contentFile;
  final String descripton;
  final String country;
  final String state;
  final String city;
  final String address;

  AddPost(this.country, this.state, this.city, this.address, {this.postType = PostType.Image, this.thumbFile, @required this.contentFile, @required this.descripton});
}

class DeletePost extends PostEvent {
  final Post post;

  DeletePost(this.post);
}

class EditPost extends PostEvent {
  final Post post;

  EditPost(this.post);
}

class UpdatePost extends PostEvent {
  final Post post;
  UpdatePost(this.post);
}

class AddBufferedPosts extends PostEvent {}