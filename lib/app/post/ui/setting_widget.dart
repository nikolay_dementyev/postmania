import 'package:flutter/material.dart';

class ChatSettingWidget extends StatelessWidget {
 
  final ValueChanged<int> onSelected;
  final int like;
  final int dislike;

  const ChatSettingWidget({Key key, this.onSelected, this.like = 0, this.dislike = 0}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const SizedBox(),
          Container(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10)),
                color: Color(0xFFD8D8D8)
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SettingItem('Comments', iconWidget: Image.asset('assets/images/icon_comment_white.png', width: 20, fit: BoxFit.fitWidth), onSelelcted: () {
                    Navigator.pop(context);
                    onSelected(0);
                  }),
                  SettingItem('Share', iconWidget: Image.asset('assets/images/icon_share.png', width: 20, fit: BoxFit.fitWidth), onSelelcted: () {
                    Navigator.pop(context);
                    onSelected(1);
                  }),
                  SettingItem('Like', iconWidget: Image.asset('assets/images/icon_thumbup.png', width: 20, fit: BoxFit.fitWidth), right: Text(like.toString(), style: TextStyle(fontSize: 16)),  onSelelcted: () {
                    Navigator.pop(context);
                    onSelected(2);
                  }),
                  SettingItem('Dislike', iconWidget: Image.asset('assets/images/icon_thumbdown.png', width: 20, fit: BoxFit.fitWidth), right: Text(dislike.toString(), style: TextStyle(fontSize: 16)), onSelelcted: () {
                    Navigator.pop(context);
                    onSelected(3);
                  }),
                ],
              ),
            ),
        ],
      ),
    );
  }
}

class SettingItem extends StatelessWidget {
  final Widget right;
  final IconData icon;
  final Widget iconWidget;
  final String title;
  final VoidCallback onSelelcted;
  const SettingItem(this.title, {Key key, this.onSelelcted, this.icon, this.right, this.iconWidget}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onSelelcted,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Row(
          children: <Widget>[
            iconWidget ?? Icon(icon, color: Colors.white),
            const SizedBox(width: 15),
            Text(title, style: TextStyle(color: Colors.black, fontFamily: 'Nexalight', fontSize: 16, fontWeight: FontWeight.w500)),
            Spacer(),
            right ?? Container()
          ],
        ),
      ),
    );
  }
}