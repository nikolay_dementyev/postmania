import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:flutter/material.dart';

class ImageScreen extends StatelessWidget {

  final String url;
  
  const ImageScreen({Key key, this.url}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SizedBox.expand(child: AsyncNetworkImage(photoKey: url, imgUrl: url, boxfit: BoxFit.cover)),
          SafeArea(
            child: Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  color: Colors.white60,
                  shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(40))
                ), child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.arrow_back_ios),
                  ),
                )),
              ),
            ),
          )
        ],
      ),
    );
  }
}