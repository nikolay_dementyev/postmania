import 'dart:io';
import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/post_model.dart';
import 'package:firebase_uploading/app/post/ui/post_input_form.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/dialogs/confirm_dialog_view.dart';
import 'package:firebase_uploading/dialogs/success_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PostAddScreen extends PostInputForm {
  
  @override
  String getSubmitButtonTitle() => 'Submit';
  
  @override 
  String getTitle() => 'Add Post';
  
  @override
  void onSubmit(BuildContext context, String country, String state, String city, String address, String description, PostType postType, File thumbFile, File contentFile) {
    if(thumbFile == null && contentFile == null) {
       showDialog<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return ConfirmDialogView(
            description: 'Are you sure you continue without image/video ?',
            leftButtonText: 'Cancel',
            rightButtonText: 'OK',
            onAgreeTap: () {
              if(!BlocProvider.of<PostBloc>(context).isNetworkAvailable) {
                showDialog(context: context, child: SuccessDialog(message: 'Please connect to the internet so post is submitted online', onConfirm: () {
                  BlocProvider.of<PostBloc>(context).add(AddPost(
                    country,
                    state,
                    city,
                    address,
                    postType: postType,
                    thumbFile: thumbFile,
                    contentFile: contentFile,
                    descripton: description
                  ));
                }));
              } else {
                BlocProvider.of<PostBloc>(context).add(AddPost(
                  country,
                  state,
                  city,
                  address,
                  postType: postType,
                  thumbFile: thumbFile,
                  contentFile: contentFile,
                  descripton: description
                ));
              }
          });
        });
    } else {
      if(!BlocProvider.of<PostBloc>(context).isNetworkAvailable) {
        showDialog(context: context, child: SuccessDialog(title: 'No internet', message: 'Please connect to the internet so post is submitted online', onConfirm: () {
          BlocProvider.of<PostBloc>(context).add(AddPost(
            country,
            state,
            city,
            address,
            postType: postType,
            thumbFile: thumbFile,
            contentFile: contentFile,
            descripton: description
          ));
        }));
      } else {
        BlocProvider.of<PostBloc>(context).add(AddPost(
          country,
          state,
          city,
          address,
          postType: postType,
          thumbFile: thumbFile,
          contentFile: contentFile,
          descripton: description
        ));
      }
    }
  }
}