import 'package:firebase_uploading/models/comment.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:firebase_uploading/widgets/read_more_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommentItem extends StatelessWidget {
  
  final Comment comment;
  final VoidCallback onLike;
  final VoidCallback onDislike;
  final VoidCallback onReply;
  
  const CommentItem({Key key, @required this.comment, this.onLike, this.onDislike, this.onReply}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    print(comment.avatar);
    return Padding(
      padding: EdgeInsets.only(left: comment.parentId == null ? 0.0 : 20.0),
      child: Card(
        margin: EdgeInsets.symmetric(vertical: 4),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(0))
        ),
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(0))
                    ),
                    elevation: 0,
                    child: Container(
                      width: 40, height: 40,
                      child: comment.avatar != null 
                      ? Image.network(comment.avatar, fit: BoxFit.cover,)
                      : Image.asset('assets/images/man.png'),
                    ),
                  ),               
                  const SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(comment.name, style: TextStyle(color: Colors.lightBlue, fontFamily: 'Nexabold')),
                            Spacer(),
                            Opacity(opacity: comment.parentId == null ? 1.0 : 0.0, child: Text(comment.createdTime, style: TextStyle(color: Colors.black))),
                          ],
                        ),
                        const SizedBox(height: 5),
                        comment.parentId == null 
                        ? Text('')
                        : Row(
                          children: <Widget>[
                            Text('replying', style: TextStyle(color: Colors.black)),
                            const SizedBox(width: 5),
                            Text(comment.oppositeName ?? '', style: TextStyle(color: Colors.lightBlue)),
                          ],
                        ) ,
                        const SizedBox(height: 5),
                        comment.parentId == null ? Container() : Text(comment.createdTime, style: TextStyle(color: Colors.black))
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 5),
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                ),
                margin: EdgeInsets.symmetric(vertical: 4),
                child: Align(alignment: Alignment.centerLeft, child: ReadMoreText(
                  comment.comment,
                  trimMode: TrimMode.Line,
                  trimLines: 3,
                  style: TextStyle(fontFamily: 'SFProText', fontSize: 14),
                )) // Text(comment.comment, style: TextStyle(fontFamily: 'SFProText'),)),
              ),
              const SizedBox(height: 5),
              Padding(
                padding: const EdgeInsets.only(left: 8),
                child: Row(
                  children: <Widget>[
                    InkWell(
                      onTap: onLike,
                      child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('assets/images/icon_thumbup.png', width: 20, fit: BoxFit.fitWidth),
                        const SizedBox(width: 30),
                        Container(width: 50, child: Text(' ${comment.likedUsers == null ? 0 : comment.likedUsers.length}', style: TextStyle(fontFamily: 'SFProText', fontSize: 14),)),
                      ],
                    ),
                  ),
                  const SizedBox(width: 20),
                  InkWell(
                    onTap: onDislike,
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset('assets/images/icon_thumbdown.png', width: 20, fit: BoxFit.fitWidth),
                        const SizedBox(width: 30),
                        Container(width: 50, child: Text(' ${comment.dislikedUsers == null ? 0 : comment.dislikedUsers.length}', style: TextStyle(fontFamily: 'SFProText', fontSize: 14),)),
                      ],
                    ),
                  ),
                  Spacer(),
                  InkWell(onTap: onReply, child: Image.asset('assets/images/icon_reply.png', width: 20, fit: BoxFit.fitWidth)),
                  const SizedBox(width: 10)
                  ],
                ),
              ),
              const SizedBox(height: 5)
            ],
          ),
        ),
      ),
    );
  }
}

class CommentList extends StatelessWidget {
  
  final List<Comment> comments;
  final Function onLike;
  final Function onDislike;
  final Function onReply;
  
  const CommentList({Key key, this.comments, this.onLike, this.onDislike, this.onReply}) : super(key: key);
  
  Column _renderComment(Comment comment) {

    List<CommentItem> commentAndReplies = [CommentItem(comment: comment, onLike: () {
      onLike(comment);
    }, 
    onDislike: () {
      onDislike(comment);
    },
    onReply: () {
      onReply(comment);
    })];
    
    if(comment.replyComments.isNotEmpty) {
      for(int i = 0; i < comment.replyComments.length; i++) {
        commentAndReplies.add(
          CommentItem(comment: comment.replyComments[i], onLike: () {
            onLike(comment.replyComments[i]);
          },
          onDislike: () {
            onDislike(comment.replyComments[i]);
          },
          onReply: () {
            onReply(comment.replyComments[i]);
          },)
        );
      }
    } 

    return Column(
      children: commentAndReplies
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(comments == null ? 0 : comments.length, (int index) {
        return _renderComment(comments[index]);
      })
    );
  }
}