import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/ui/post_detail.dart';
import 'package:firebase_uploading/app/post/ui/post_item.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/providers/comment_provider.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PostList extends StatefulWidget {
  final bool isShowSearchBar;

  const PostList({Key key, this.isShowSearchBar}) : super(key: key);

  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  
  @override
  void initState() {
    super.initState();
    
    BlocProvider.of<PostBloc>(context).add(FetchPosts());
  }
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PostBloc, PostState>(
      condition: ((prev, state) {
        return state is PostListChanged;
      }),
      builder: (context, state) {
        return PostListScreen(
          state is PostListChanged ? state.postList : [],
          widget.isShowSearchBar
        );
      },
    );
  }
}

class PostListScreen extends BaseListScreen {
  
  final List<Post> postList;
  final bool isShowSearchBar;
  
  PostListScreen(this.postList, this.isShowSearchBar);
  
  @override
  bool showSearchBar() {
    return isShowSearchBar;
  }
  
  @override
  int getCount() {
    return postList.length;
  }
  
  @override
  Widget buildListItem(BuildContext context, int index) {
    return PostItem(post: postList[index]);
  }
  
  @override
  void onSelected(BuildContext context, int index) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PostDetail(Provider.of<CommentProvider>(context), post: postList[index])));
  }
  
  @override
  void onSearch(String query, BuildContext context) {
    BlocProvider.of<PostBloc>(context).add(FetchPosts(query: query));
  }
  
}