import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:flutter/material.dart';

class PostItem extends StatelessWidget {
  
  final Post post;
  final bool isEdit;
  
  final VoidCallback onDelete;
  final VoidCallback onEdit;

  const PostItem({Key key, this.post, this.isEdit = false, this.onDelete, this.onEdit}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(0))),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: IntrinsicHeight(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              post.thumbnailUrl == null || post.thumbnailUrl.isEmpty ? 
              Container(color: Color(0xFFFDEE39), child: Image.asset('assets/images/default_upload.png', width: 100, height: 100, fit: BoxFit.contain))
              : AsyncNetworkImage(
                photoKey: post.thumbnailUrl + 'list_item',
                imgUrl: post.thumbnailUrl,
                width: 100,
                height: 100,
                boxfit: BoxFit.cover,
              ),
              const SizedBox(width: 15),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Expanded(child: Text(post.description, maxLines: 5, style: TextStyle(color: Colors.black, fontSize: 15, height: 1.1),)),
                        ],
                      ),
                    ),
                    const SizedBox(height: 5),
                    Row(
                      children: <Widget>[
                        Text(post.createdTime, style: TextStyle(fontSize: 14, color: Colors.lightBlue)),
                        Spacer(),
                        Opacity(opacity: isEdit ? 1.0 : 0.0, child: InkWell(onTap: onEdit, child: Text('Edit', style: TextStyle(fontSize: 16, color: Colors.red)))),
                        const SizedBox(width: 10),
                        Opacity(opacity: isEdit ? 1.0 : 0.0, child: InkWell(onTap: onDelete, child: Text('Delete', style: TextStyle(fontSize: 16, color: Colors.red)))),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}