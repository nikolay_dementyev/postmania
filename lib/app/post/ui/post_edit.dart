import 'dart:io';
import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/post_model.dart';
import 'package:firebase_uploading/app/post/ui/post_input_form.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/utils/firebase/firestorage.dart';
import 'package:firebase_uploading/utils/firebase/firestore.dart';
import 'package:flutter/cupertino.dart';

class PostEditScreen extends PostInputForm {
  
  final Post post;
  
  PostEditScreen(this.post);
  
  @override
  void onSubmit(BuildContext context, String country, String state, String city, String address, String description, PostType postType, File thumbFile, File contentFile) async {
    /// First Do operation on attach files
    BlocProvider.of<SubmitBloc>(context).add(Submit());

    try {
      if(contentFile != null) {
        if(post.postUrl != null) {
          await FireStorage.deleteFile(post.postUrl);
          if(post.postType == PostType.Video) {
            await FireStorage.deleteFile(post.thumbnailUrl);
          }
        }
        String postUrl = await FireStorage.saveAttachFile(contentFile);
        String thumbnailUrl = postUrl;
        if(postType == PostType.Video) {
          thumbnailUrl = await FireStorage.saveAttachFile(thumbFile);
        }
        post.postUrl = postUrl;
        post.thumbnailUrl = thumbnailUrl;
      }
      
      post.postType = postType;
      post.country = country;
      post.region = state;
      post.city = city;
      post.address = address;
      post.description = description;
      
      await FireStore.updatePost(post);
      BlocProvider.of<SubmitBloc>(context).add(SubmitSucceeded());
      Global().showToastMessage('Edited Post');
      BlocProvider.of<PostBloc>(context).add(EditPost(post));
      Navigator.pop(context, post);
    } catch (e) {
      BlocProvider.of<SubmitBloc>(context).add(SubmitFailed());
      Global().showToastMessage('Failed to Edit');
    }
  }
  
  @override
  PostType getPostType() => post.postType;
  
  @override
  String getTitle() {
    return 'Edit Post';
  }
  
  @override
  String getSubmitButtonTitle() {
    return 'Edit';
  }

  @override
  String getDescription() {
    return post.description;
  }

  @override
  String getNetworkImage() => post.thumbnailUrl;

  @override
  String getCountry() => post.country;

  @override
  String getState() => post.region;

  @override
  String getCity() => post.city;
  
  @override
  String getAddress() => post.address;
}