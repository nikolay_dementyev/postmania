import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_uploading/app/location_pick/location_pick.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/helpers/index.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mdi/mdi.dart';

abstract class PostInputForm extends StatefulWidget {
  
  @override
  _PostInputFormState createState() => _PostInputFormState();
  
  void onSubmit(
    BuildContext context,
    String country,
    String state,
    String city,
    String address,
    String description,
    PostType postType,
    File thumbFile,
    File contentFile,
  );
  
  String getTitle();
  
  String getSubmitButtonTitle();
  
  PostType getPostType() => null;
  
  String getDescription() => '';
  
  String getCountry() => null;
  
  String getState() => null;
  
  String getCity() => null;
  
  String getAddress() => '';
  
  String getNetworkImage() => null; 
}

class _PostInputFormState extends State<PostInputForm> {
  
  PostType _postType;
  File _thumbFile;
  File _contentFile;
  String _descripton = '';
  
  String _currentCountry;
  String _currentRegion;
  String _currentCity;
  String _currentAddress;
  
  _onImagePicked() {
    pickImage(ImageSource.gallery).then((image) {
      if(image == null) return;
      _postType = PostType.Image;
      setState(() {
        _thumbFile = image;
        _contentFile = image;
      });
    });
  }
  
  _onVideoPicked() async {
    pickFile(pickingType: FileType.VIDEO, onPickedFile: (path) async {
      if(path == null) return;
      
      _contentFile = File(path);
      int count = _contentFile.lengthSync();
      if(count > 300 * 1024 * 1024) {
        Global().showToastMessage('Video should be smaller than 300MB');
        return;
      }
      _postType = PostType.Video;
      File thumbFile = await generateVideoThumbnail(path);
      setState(() {
        _thumbFile = thumbFile;
      });
    });
  }
  
  _onSubmit() {
    widget.onSubmit(context, _currentCountry, _currentRegion, _currentCity, _currentAddress, _descripton, _postType, _thumbFile, _contentFile);
  }
  
  @override
  void initState() {
    super.initState();
    
    _postType = widget.getPostType();
    _descripton = widget.getDescription();
    _currentCountry = widget.getCountry();
    _currentRegion = widget.getState();
    _currentCity = widget.getCity();
    _currentAddress = widget.getAddress();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(widget.getTitle()),
        centerTitle: true,
        leading: InkWell(onTap: () {
          Navigator.pop(context);
        }, child: Icon(Icons.arrow_back_ios)),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AspectRatio(
              aspectRatio: 5/3,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  _thumbFile != null ? Image.file(_thumbFile, fit: BoxFit.cover) : (
                    widget.getNetworkImage() == null 
                    ? Container(
                      color: Color(0xFFB193F4),
                    )
                    : AsyncNetworkImage(
                      photoKey: widget.getNetworkImage(),
                      imgUrl: widget.getNetworkImage(),
                      boxfit: BoxFit.cover,
                    )
                  ),
                  Container(
                    color: Color(0x33FFFFFF),
                    child: InkWell(
                      onTap: () {
                        showCupertinoModalPopup(
                          builder: ((context) {
                            return CupertinoActionSheet(
                              title: Text('Choose File'),
                              message: Text('Please choose file type to upload'),
                              actions: <Widget>[
                                CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    _onImagePicked();
                                  },
                                  child: Text('Image'),
                                ),
                                CupertinoActionSheetAction(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    _onVideoPicked();
                                  },
                                  child: Text('Video'),
                                )
                              ],
                              cancelButton: CupertinoActionSheetAction(
                                child: Text('Cancel'),
                                onPressed: () {
                                  Navigator.pop(context);
                                }
                              ),
                            );
                          }), context: context
                        );
                      },
                      child: _thumbFile == null && widget.getNetworkImage() == null ? Container() : Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(Mdi.camera, color: Colors.black54, size: 50),
                            Text('Tap here to add\nImage or Video')
                          ],
                        ),
                      )
                    )
                  )
                ]
              )
            ),
            Padding(
              padding: const EdgeInsets.only(left: 10, top: 10),
              child: TextArea(
                initialValue: _descripton,
                hint: 'Description',
                showLabel: false,
                onChanged: (value) {
                  _descripton = value;
                },
              ),
            ),
            LocationPick(country: _currentCountry, state: _currentRegion, city: _currentCity, address: _currentAddress, onLocationChanged: (country, state, city, address) {
              _currentCountry = country;
              _currentRegion = state;
              _currentCity = city;
              _currentAddress = address;
            }),
           BlocBuilder<SubmitBloc, SubmitState>(
             builder: (context, state) {
               return  InkWell(
                onTap: state is Submitting ? null : _onSubmit,
                child: Container(
                  color: Colors.orange,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  child: Center(
                    child: state is Submitting ? LoadingBar() : Text(widget.getSubmitButtonTitle(), style: TextStyle(fontSize: 22, fontFamily: 'Nexabold', color: Colors.white)),
                  ),
                ),
              );
             },
           ),
            const SizedBox(height: 15),
          ],
        ),
      ),
    );
  }
}