import 'dart:async';
import 'package:firebase_uploading/app/home/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/ui/post_add.dart';
import 'package:firebase_uploading/app/post/ui/setting_widget.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/dialogs/result_dialog.dart';
import 'package:firebase_uploading/dialogs/success_dialog.dart';
import 'package:firebase_uploading/models/comment.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/providers/comment_provider.dart';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:firebase_uploading/widgets/video_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:neeko/neeko.dart';
import 'package:share/share.dart';
import 'comment_list.dart';
import 'package:meta/meta.dart';
import 'image_screen.dart';
import 'package:native_device_orientation/native_device_orientation.dart';

class PostDetail extends StatefulWidget {
  
  final Post post;
  final CommentProvider commentProvider;
  
  const PostDetail(this.commentProvider, {Key key, @required this.post}) : super(key: key);
  @override
  _PostDetailState createState() => _PostDetailState();
}

class _PostDetailState extends State<PostDetail> {
  
  int _keyboardSubscribeId;
   
  int orientation = 0;
  
  bool showMore = false;
  bool isCommentTyping = false;
  
  Profile profile;
  
  NeekoPlayerController _controller;
  
  FocusNode _focusNode = FocusNode();
  TextEditingController _commentController;
  
  String myId;
  
  _share() {
    Share.share(widget.post.postUrl);
  }
  
  _replyComment() {
    setState(() {
      isCommentTyping = true;
    });
    Timer(Duration(milliseconds: 100), () {
      _focusNode.requestFocus();
    });
  }
  
  _submitComment() {
    
    if(_commentController.text.isEmpty) {
      Global().showToastMessage('Please input comments');
      return;
    }
    
    setState(() {
      isCommentTyping = false;
    });
    
    widget.commentProvider.addNewComment(_commentController.text);

    _commentController.text = '';
    showDialog(context: context, child: SuccessDialog(message: 'Submitted Comment!'));
    // ResultDialog(context, message: 'Submitted Comment!!!').show();
  }
  
  _like() {

    if(widget.post.likedUsers != null && widget.post.likedUsers.contains(myId)) return;
    
    if(widget.post.likedUsers == null) {
      widget.post.likedUsers = [myId];
    } else {
      widget.post.likedUsers.add(myId);
    }
    
    if(widget.post.dislikedUsers != null && widget.post.dislikedUsers.isNotEmpty) {
      if(widget.post.dislikedUsers.contains(myId)) {
        widget.post.dislikedUsers.remove(myId);
      }
    }
    
    setState(() {
      
    });
    
    BlocProvider.of<PostBloc>(context).add(UpdatePost(widget.post));
  }
   
  _dislike() {
    if(widget.post.dislikedUsers != null && widget.post.dislikedUsers.contains(myId)) return;
    
    if(widget.post.dislikedUsers == null) {
      widget.post.dislikedUsers = [myId];
    } else {
      widget.post.dislikedUsers.add(myId);
    }
    
    if(widget.post.likedUsers != null && widget.post.likedUsers.contains(myId)) {
      widget.post.likedUsers.remove(myId);
    }
    
    setState(() {
      
    });
    
    BlocProvider.of<PostBloc>(context).add(UpdatePost(widget.post));
  }
  
  _showBottomSetting(BuildContext context) {
    showBottomSheet(context: context, backgroundColor: Colors.transparent, builder: (context) {
      return ChatSettingWidget(onSelected: (int index) {
        if(index == 0) {
          _replyComment();
        } else if(index == 1) {
          _share();
        } else if(index == 2) {
          _like();
        } else if(index == 3) {
          _dislike();
        }
      }, like: widget.post.likedUsers == null ? 0 : widget.post.likedUsers.length, dislike: widget.post.dislikedUsers == null ? 0 : widget.post.dislikedUsers.length,);
    });
  }
  
  @override
  void initState() {
    super.initState();
    
    _commentController = TextEditingController();
    profile = BlocProvider.of<AuthenticationBloc>(context).state.profile;
    myId = BlocProvider.of<AuthenticationBloc>(context).state.profile.userId;
    
    widget.commentProvider.fetchComments(widget.post.id);
    
    _keyboardSubscribeId = KeyboardVisibilityNotification().addNewListener(
      onChange: (value) {
        print(value);
      },
      onHide: () {
        if(isCommentTyping) {
          setState(() {
            isCommentTyping = false;
            widget.commentProvider.deselectComment();
          });
        }
      }
    );
  }
  
  @override
  void dispose() {
    KeyboardVisibilityNotification().removeListener(_keyboardSubscribeId);
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return NativeDeviceOrientationReader(
      useSensor: true,
      builder: (context) {
        NativeDeviceOrientation orientation = NativeDeviceOrientationReader.orientation(context);
        print(orientation.index);
        if(orientation.index != this.orientation) {
          this.orientation = orientation.index;
          
          if(orientation.index == 2 || orientation.index == 3) {
            Timer(Duration(milliseconds: 300), () {
              _controller?.enterFullScreen();
            });
          } 
        }
        
        return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: Text('Post Detail'),
            centerTitle: true,
            leading: InkWell(onTap: () {
              Navigator.pop(context);
            }, child: Icon(Icons.arrow_back_ios)),
          ),
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              if(widget.post.postType == PostType.Image) {
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ImageScreen(url: widget.post.postUrl)));
                              }
                            },
                            child: Container(
                              child: AspectRatio(
                                aspectRatio: 5 / 3,
                                child:
                                Stack(
                                  fit: StackFit.expand,
                                  children: <Widget>[
                                    widget.post.thumbnailUrl == null || widget.post.thumbnailUrl.isEmpty ?
                                    Image.asset('assets/images/default_upload.png', fit: BoxFit.cover)
                                    : widget.post.postType == PostType.Image 
                                    ? AsyncNetworkImage(photoKey: widget.post.thumbnailUrl, imgUrl: widget.post.thumbnailUrl, boxfit: BoxFit.cover,)
                                    : VideoPlayer(url: widget.post.postUrl, thumbnail: widget.post.thumbnailUrl, onPlayerCreated: (controller) {
                                      _controller = controller;
                                      if(this.orientation ==  2 || this.orientation == 3) {
                                        _controller.enterFullScreen();
                                      }
                                    }),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Align(
                                        alignment: Alignment.topRight,
                                        child: Builder(
                                          builder: (context) {
                                            return InkWell(
                                              onTap: () {
                                                _showBottomSetting(context);
                                              },
                                              child: Icon(Icons.more_vert, color: Colors.lightBlue),
                                            );
                                          }
                                        )
                                      )
                                    )
                                  ]
                                )
                              ),
                            ),
                          ),
                          const SizedBox(height: 5),
                          Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8, top: 8),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text('By ',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                        Text(widget.post.posterName ?? 'Developer',
                                            style: TextStyle(
                                                color: Colors.lightBlue,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                      const SizedBox(height: 10),
                                      Wrap(
                                        children: <Widget>[
                                          Text('${widget.post.city}, '),
                                          Text('${widget.post.country}'),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Align(
                                    alignment: Alignment.bottomRight,
                                    child: Builder(
                                      builder: (context) {
                                        DateTime createdTime = widget.post.timeStamp.toDate().toLocal();
                                        DateFormat dateFormat = DateFormat("MMM dd, yyyy hh:mm a");
                                        return Text(dateFormat.format(createdTime));
                                      },
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 12, right: 12),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                const SizedBox(height: 10),
                                Text(
                                  widget.post.description,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontFamily: 'SFProText')
                                  ),
                                const SizedBox(height: 10),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 5),
                                  child: Row(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: _like,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Image.asset('assets/images/icon_thumbup.png', width: 20, fit: BoxFit.fitWidth),
                                            const SizedBox(width: 30),
                                            Container(width: 50, child: Text(' ${widget.post.likedUsers == null ? 0 : widget.post.likedUsers.length}', style: TextStyle(fontFamily: 'SFProText', fontSize: 14),)),
                                          ],
                                      ),
                                      ),
                                      const SizedBox(width: 20),
                                      InkWell(
                                        onTap: _dislike,
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.asset('assets/images/icon_thumbdown.png', width: 20, fit: BoxFit.fitWidth),
                                            const SizedBox(width: 30),
                                            Container(width: 50, child: Text(' ${widget.post.dislikedUsers == null ? 0 : widget.post.dislikedUsers.length}', style: TextStyle(fontFamily: 'SFProText', fontSize: 14),)),
                                          ],
                                        ),
                                      ),
                                      Spacer(),
                                      InkWell(onTap: _share, child: Image.asset('assets/images/icon_share.png', width: 20, fit: BoxFit.fitWidth)),
                                      const SizedBox(width: 10)
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 5),
                                Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 0),
                                  child: Column(children: <Widget>[
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      const SizedBox(height: 5),
                                      CommentList(
                                        comments: widget.commentProvider.comments != null && widget.commentProvider.comments.length > 0 ? widget.commentProvider.comments : [],
                                        onLike: (Comment comment) {
                                          widget.commentProvider.like(comment, profile.userId);
                                        },
                                        onDislike: (Comment comment) {
                                          widget.commentProvider.dislike(comment, profile.userId);
                                        },
                                        onReply: (Comment comment) {
                                          /// Notify which comment is selected for reply
                                          widget.commentProvider.selectComment(comment);
                                          _replyComment();
                                        },
                                      ),
                                      const SizedBox(height: 5),
                                    ])
                                  ]),
                                ),
                                const SizedBox(height: 5),
                                
                                const SizedBox(height: 5)
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5),
                    color: Color(0xFFDDDDDD),
                    child: Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text('${(widget.post.likedUsers == null ? 0 : widget.post.likedUsers.length) + (widget.post.dislikedUsers == null ? 0 : widget.post.dislikedUsers.length)}', style: TextStyle(color: Colors.lightBlue, fontSize: 16),),
                          const SizedBox(width: 15),
                          Builder(
                            builder: (context1) {
                              return InkWell(onTap: () {
                                _showBottomSetting(context1);
                                }, child: Icon(Icons.favorite, color: Colors.blue, size: 40,));
                              },
                          ),
                          const SizedBox(width: 40),
                          FloatingActionButton(
                            child: Icon(Icons.add),
                            onPressed: () {
                              Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PostAddScreen()));
                            },
                          ),
                          const SizedBox(width: 40),
                          InkWell(onTap: _replyComment, child: Image.asset('assets/images/icon_comment.png', width: 30, fit: BoxFit.fitWidth)),
                          const SizedBox(width: 15),
                          Text('${widget.commentProvider.comments == null ? 0 : widget.commentProvider.comments.length}', style: TextStyle(color: Colors.lightBlue, fontSize: 16)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              isCommentTyping 
              ? Container(
                color: Colors.white,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(),
                    ),
                    Expanded(
                      flex: 7,
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black54, width: 5.0)
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 15),
                                child: TextFormField(
                                  controller: _commentController,
                                  focusNode: _focusNode,
                                  maxLength: 500,
                                  onChanged: (value) {
                                    setState(() {
                                      
                                    });
                                  },
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    counterText: null,
                                    hintText: 'Comments here...',
                                    hintStyle: TextStyle(fontSize: 18, fontFamily: 'Nexabold'),
                                    counter: SizedBox(height: 0)
                                  ),
                                  maxLines: null,
                                  style: TextStyle(fontSize: 16, height: 1.3),
                                ),
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                const SizedBox(width: 15),
                                Text('${_commentController.text.length}/500'),
                                Spacer(),
                                InkWell(
                                  onTap: _submitComment,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Icon(Icons.send),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        )
                      ),
                    ),
                  ],
                ),
              )
              : Container()
            ],
          ),
        );
      },
    );
  }
}