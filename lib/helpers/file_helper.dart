import 'dart:io';
import 'package:firebase_uploading/singletons/index.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
// import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

/// Copy db file in the asset to the application directory
/// value of assetPath variable : 'assets/db/app.db'
/// target directory : application_directory/app.db
/// if app.db already exists, then does not copy.
Future copyAssetDbToApplicationDirectory(String assetPath) async {
  Directory directory = await getApplicationDocumentsDirectory();
  var dbPath = join(directory.path, "app.db");
  if (FileSystemEntity.typeSync(dbPath) == FileSystemEntityType.notFound) {
    ByteData data = await rootBundle.load(assetPath);
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await File(dbPath).writeAsBytes(bytes);
  }
}

Future<String> _getTempDirectory() async {
  Directory tempDir = await getTemporaryDirectory();
  String tempPath = tempDir.path;
  return tempPath;
}

/// Get File Object from file in asset folder
Future<File> getFileFromAsset(String path) async {
  Directory tempDir = await getTemporaryDirectory();
  String tempPath = tempDir.path;
  ByteData data = await rootBundle.load(path);
  File file = File(tempPath + path.split('/').last);
  List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
  await file.writeAsBytes(bytes);
  return file;
}

/// Pick file of any type in mobile device
void pickFile({FileType pickingType = FileType.ANY, bool multiPick = false, Function(String) onPickedFile, Function(Map<String, String>) onPickedFiles, Function onError}) async {
   try {
    if(multiPick) {          
      Map<String, String> paths = await FilePicker.getMultiFilePath(
        type: pickingType,
      );
      if(onPickedFiles != null) {
        onPickedFiles(paths);
      }
    } else {         
      String path = await FilePicker.getFilePath(
        type: pickingType,
        // fileExtension: _extension
      );
      if(onPickedFile != null) {
        onPickedFile(path);
      }
    } 
  } on PlatformException catch (e) {
    if(onError != null) {
      onError(e);
    }
  }
}

Future<File> pickImage(ImageSource imageSource, {bool allowCroping = true}) async {

   File image = await ImagePicker.pickImage(source: imageSource);
   
   if(image == null) {
     Global().showToastMessage('You did not select image');
     return null;
   }
   if(allowCroping) {
     File croppedImage = await ImageCropper.cropImage(sourcePath: image.path,
      maxWidth: 512,
      maxHeight: 512,
    );
    return croppedImage;
   }
   return image;
}

Future<File> generateVideoThumbnail(String videoPath) async {
  final uint8list = await VideoThumbnail.thumbnailData(
    video: videoPath,
    imageFormat: ImageFormat.PNG,
    maxWidth: 1000, // specify the width of the thumbnail, let the height auto-scaled to keep the source aspect ratio
    quality: 100,
  );
  File file = File(await _getTempDirectory() + DateTime.now().toString() + ".png");
  file.writeAsBytesSync(uint8list);
  return file;
}