import 'dart:convert';
import 'dart:io';
import 'dart:math';
// import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
// import 'package:qr_flutter/qr_flutter.dart';
import 'package:password_strength/password_strength.dart';

bool isValideEmail(String email) {
  return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);
}

bool isStrongPassword(String password) {
  double strength = estimatePasswordStrength(password);
  print(strength);
  if(strength >= 0.5) {
    return  true;
  } else {
    return false;
  }
}

/// Remove First Characto Of String
String dropFirst({str = String}) {
  return str.toString().substring(1);
}

/// Extract file extension from file object
String getExtension(File file) {
  String path = file.path;
  var splits = path.split('.');
  return splits.last;
}

/// Extract file extension from filename
String getExtensionWithPath(String fileName) {
  var splits = fileName.split('.');
  return splits.last;
}

String randomString({length: int}) {
    const chars = "abcdefghijklmnopqrstuvwxyz";
    Random rnd = new Random(new DateTime.now().millisecondsSinceEpoch);
    String result = "";
    for (var i = 0; i < length; i++) {
      result += chars[rnd.nextInt(chars.length)];
    }
    return result.toUpperCase();
  }

/// Generate Base64 String From File
String base64StringFromFile(File imageFile) {
  List<int> imageBytes = imageFile.readAsBytesSync();
  String base64Image = base64Encode(imageBytes);
  return base64Image;
}

/// Generate Image From Base64 String
Image imageFromBase64String(String base64String) {
  return Image.memory(base64Decode(base64String));
}

/// Extract Bin Number From CardNumber
String extractBinFromCardNumber(String cardNumber) {
  if(cardNumber == null || cardNumber == '' || cardNumber.length < 16) return null;

  return cardNumber.substring(0, 6);
}

/// Make first character capital
String capitalizeStartCharacter(String s) => s[0].toUpperCase() + s.substring(1);