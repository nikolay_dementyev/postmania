import 'package:firebase_uploading/models/base_model.dart';

///     Clone Object Array With BaseModel Type
///     Usage :
/// 
///     class User extends BaseModel {
///       String name;
///       String email;
///  
///       User({this.name, this.email});
///  
///       @override
///       BaseModel copyWith() {
///         return User(email: this.email, name: this.name);
///       }
///     }
///     
///     List<User> userList = [
///       User(name: 'nikolay', email: 'nikolay@yandex.com'),
///       User(name: 'sergey', email: 'sergey@yandex.com'),
///     ];
///     
///     List<User> cloneList = clone<User>(userList);
///     01/15/2020 00:17 By Jonh

List clone<T>(List<BaseModel> orignalArray) {
  if(orignalArray == null) return null;
  List<T> cloneList = [];
  for(int i = 0; i < orignalArray.length; i++) {
    cloneList.add(orignalArray[i].copyWith() as T);
  }
  return cloneList;
}