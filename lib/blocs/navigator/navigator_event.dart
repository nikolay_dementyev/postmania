import 'package:equatable/equatable.dart';

abstract class NavigatorEvent extends Equatable {
  const NavigatorEvent();

  @override
  List<Object> get props => null;
}

class GoLoginPage extends NavigatorEvent {}
class GoHome extends NavigatorEvent {}
class GoBack extends NavigatorEvent {}