import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_uploading/app/login/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_uploading/app/home/ui/home_page.dart';
import './bloc.dart';

class NavigatorBloc extends Bloc<NavigatorEvent, dynamic> {
  
  final GlobalKey<NavigatorState> navigatorKey;

  NavigatorBloc({@required this.navigatorKey}) : assert(navigatorKey != null);
  
  @override
  dynamic get initialState => null;
  
  @override
  Stream<dynamic> mapEventToState(
    NavigatorEvent event,
  ) async* {
    if(event is GoHome) {
      navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => HomePage()),    
        ModalRoute.withName('/home')); 
    } else if(event is GoBack) {
      navigatorKey.currentState.pop();
    } else if(event is GoLoginPage) {
      navigatorKey.currentState.pushAndRemoveUntil( 
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),    
        ModalRoute.withName('/login')); 
    }
  }
}