import 'package:equatable/equatable.dart';

abstract class TimerState extends Equatable {
  const TimerState();
  @override
  List<Object> get props => null;
}

class InitialTimerState extends TimerState {
  @override
  List<Object> get props => [];
}

class Ticked extends TimerState {
  final int ticks;
  
  Ticked(this.ticks);
}

class Running extends TimerState {}

class Stopped extends TimerState {}