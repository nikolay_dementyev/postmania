import 'package:equatable/equatable.dart';

abstract class SubmitState extends Equatable {
  const SubmitState();

  @override
  List<Object> get props => null;
}

class InitialSubmitState extends SubmitState {
  @override
  List<Object> get props => [];
}

class Submitting extends SubmitState {}

class SubmitSuccess extends SubmitState {}

class SubmitFailure extends SubmitState {}