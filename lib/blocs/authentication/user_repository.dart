import 'package:shared_preferences/shared_preferences.dart';
import '../../models/index.dart';

class UserRepository {
  
  final SharedPreferences sharedPrefs;
  
  UserRepository(this.sharedPrefs);
  
  bool isLogedIn() {
    return sharedPrefs.getBool('logged_in') ?? false;
  }
  
  Profile loadSession() {
    
    String userId = sharedPrefs.getString('user_id');
    String userName = sharedPrefs.getString('user_name');
    String email = sharedPrefs.getString('email');
    String firstName = sharedPrefs.getString('first_name');
    String lastName = sharedPrefs.getString('last_name');
    String stateName = sharedPrefs.getString('state');
    String avatar = sharedPrefs.getString('avatar');
    String aboutMe = sharedPrefs.getString('about_me');
    String city = sharedPrefs.getString('city');
    String country = sharedPrefs.getString('country');
    
    Profile profile = Profile(userId: userId, avatarUrl: avatar, firstName: firstName, lastName: lastName, stateName: stateName, aboutMe: aboutMe, cityName: city, countryName: country, userName: userName, email: email);
    return profile;
  }
  
  void deleteSession() {
    sharedPrefs.setString('first_name', null);
    sharedPrefs.setString('last_name', null);
    sharedPrefs.setString('state', null);
    sharedPrefs.setString('avatar', null);
    sharedPrefs.setString('about_me', null);
    sharedPrefs.setString('city', null);
    sharedPrefs.setString('country', null);
    sharedPrefs.setString('user_id', null);
    sharedPrefs.setBool('logged_in', false);
    sharedPrefs.setString('user_name', null);
    sharedPrefs.setString('email', null);
  }
  
  void saveSession(Profile profile) {
    sharedPrefs.setString('first_name', profile.firstName);
    sharedPrefs.setString('last_name', profile.lastName);
    sharedPrefs.setString('state', profile.stateName);
    sharedPrefs.setString('avatar', profile.avatarUrl);
    sharedPrefs.setString('about_me', profile.aboutMe);
    sharedPrefs.setString('city', profile.cityName);
    sharedPrefs.setString('country', profile.countryName);
    sharedPrefs.setBool('logged_in', true);
    sharedPrefs.setString('user_name', profile.userName);
    sharedPrefs.setString('email', profile.email);
    sharedPrefs.setString('user_id', profile.userId);
  }
}