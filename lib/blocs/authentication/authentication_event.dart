import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import '../../models/index.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();
  
  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {}

class CheckAuthenticationStatus extends AuthenticationEvent {}

class LoggedIn extends AuthenticationEvent {
  
  final Profile profile;
  
  const LoggedIn({@required this.profile});
  
  @override
  List<Object> get props => [];

  @override
  String toString() => 'LoggedIn { token: }';
}

class ChangeProfile extends AuthenticationEvent {
  final Profile profile;

  ChangeProfile(this.profile);
}

class LoggedOut extends AuthenticationEvent {}