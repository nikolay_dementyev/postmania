import 'package:equatable/equatable.dart';

abstract class LoadEvent extends Equatable {
  const LoadEvent();

  @override
  List<Object> get props => null;
}

class Load extends LoadEvent {} 

class LoadSucceeded extends LoadEvent {} 

class LoadFailed extends LoadEvent {}