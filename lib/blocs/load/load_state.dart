import 'package:equatable/equatable.dart';

abstract class LoadState extends Equatable {
  const LoadState();

  @override
  List<Object> get props => null;
}

class InitialLoadState extends LoadState {
  @override
  List<Object> get props => [];
}

class Loading extends LoadState {}

class LoadSuccess extends LoadState {}

class LoadFailure extends LoadState {}