import 'package:firebase_uploading/widgets/video_player.dart';
import 'package:flutter/material.dart';

class VideoTest extends StatefulWidget {
  @override
  _VideoTestState createState() => _VideoTestState();
}

class _VideoTestState extends State<VideoTest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: VideoPlayer(
        url: 'http://192.168.208.80/181016_02_Grenada_14.mp4',
        title: '181016_02_Grenada_14.mp4',
      ),
    );
  }
}