import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/providers/comment_provider.dart';
import 'package:firebase_uploading/providers/profile_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_uploading/app.dart';
import 'blocs/index.dart';

void main() async {
  
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  final userRepository = UserRepository(sharedPreferences);
  final GlobalKey<NavigatorState> _navigatorKey = GlobalKey();
  
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  
  runApp(
   MultiBlocProvider(
      providers: [
        BlocProvider<NavigatorBloc>(
          create: (context) => NavigatorBloc(navigatorKey: _navigatorKey),
        ),
        BlocProvider<AuthenticationBloc>(
          create: (context) => AuthenticationBloc(userRepository, navigatorBloc: BlocProvider.of<NavigatorBloc>(context))..add(AppStarted()),
          // create: (context) => AuthenticationBloc(userRepository, navigatorBloc: BlocProvider.of<NavigatorBloc>(context)),
        ),
        BlocProvider<LoadBloc>(
          create: (context) => LoadBloc(),
        ),
        BlocProvider<SubmitBloc>(
          create: (context) => SubmitBloc(),
        ),
      ],
      child: BlocProvider(
        create: (context) => PostBloc(
          submitBloc: BlocProvider.of<SubmitBloc>(context),
          navigatorBloc: BlocProvider.of<NavigatorBloc>(context),
          authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
          loadBloc: BlocProvider.of<LoadBloc>(context)
        )..add(Initialize()),
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider<CommentProvider>(create: (context) => CommentProvider(
              BlocProvider.of<AuthenticationBloc>(context)
            )),
          ],
          child: MyApp(navigatorKey: _navigatorKey),
        )
      ),
    )
  );
}