import 'package:firebase_uploading/dialogs/base_dialog.dart';
import 'package:flutter/material.dart';

enum MessageType {
  SUCCESS,
  WARNING,
  ERROR
}

class ResultDialog extends BaseDialog {

  final String message;
  final MessageType type;
  final BuildContext context;
  
  ResultDialog(this.context, {this.message, this.type = MessageType.SUCCESS}) : super(context: context);
  
  Color _getColor() {
    switch(type) {
      case MessageType.SUCCESS:
        return Colors.green;
      case MessageType.WARNING:
        return Colors.orange;
      case MessageType.ERROR:
        return Colors.red;
    }
    return Colors.white;
  }
  
  String _getTitle() {
     switch(type) {
      case MessageType.SUCCESS:
        return 'Thank you!';
      case MessageType.WARNING:
        return 'Warning';
      case MessageType.ERROR:
        return 'Oops';
    }
    return '';
  }

  @override
  double get width => 350;
  
  @override
  Widget buildBody() {
    return Card(
      elevation: 30,
      color: _getColor(),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(15))
      ),
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(_getTitle(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 30)),
            const SizedBox(height: 40),
            Text(message, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 22)),
            const SizedBox(height: 25),
            InkWell(
              onTap: () {
                dismiss();
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white, width: 1.0),
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  color: Color(0x66000000)
                ),
                margin: EdgeInsets.symmetric(horizontal: 20),
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Center(
                  child: Text('OK', style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}