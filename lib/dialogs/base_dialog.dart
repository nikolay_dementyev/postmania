import 'package:flutter/material.dart';

abstract class BaseDialog extends StatelessWidget {
  
  final BuildContext context;
  final double width;
  
  BaseDialog({@required this.context, this.width = 300});
  
  Widget buildBody();
  
  show() {
    showDialog(
        context: context,
        builder: (context) => Container(
            child: this
        )
    );
  }
  
  dismiss() {
    Navigator.pop(context);
  }
  
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: () {
          dismiss();
        },
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          color: Colors.transparent,
          child: Center(
            child: Container(
              color: Colors.transparent,
              width: width,
              child: buildBody()
            ),
          ),
        ),
      )
    );
  }
}