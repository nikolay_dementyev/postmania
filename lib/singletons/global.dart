
import 'dart:io';
import 'package:connectivity/connectivity.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class Global {

  static final Global _instance = Global._internal();
  
  factory Global() {
    return _instance;
  }
  
  Global._internal();
  
  void hideSoftKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }
  
  Future<bool> checkPermission(PermissionGroup permission) async {
    PermissionStatus permissionStatus = await PermissionHandler().checkPermissionStatus(permission);
    if(permissionStatus != PermissionStatus.granted) {
      Map<PermissionGroup, PermissionStatus> result = await PermissionHandler().requestPermissions([permission]);
      PermissionStatus locationPermissionStatus = result[PermissionGroup.location];
      if(locationPermissionStatus == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }
  
  Future<bool> isInternetAvailable() async {
    final ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.mobile) {
      // print('Mobile');
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      // print('Wifi');
      return true;
    } else if (connectivityResult == ConnectivityResult.none) {
      return false;
    } else {
      return false;
    }
  }
  
  void showToastMessage(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Color.fromRGBO(30, 30, 30, 0.6),
        textColor: Colors.white
    );
  }
  
  void showSnackBar(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));
    Scaffold.of(context).showSnackBar(snackBar);
  }
  
  Future<bool> sendEmail(String os, String reason, String description) async {
    String version;
    if (Platform.isAndroid) {
      var androidInfo = await DeviceInfoPlugin().androidInfo;
      var release = androidInfo.version.release;
      var sdkInt = androidInfo.version.sdkInt;
      var manufacturer = androidInfo.manufacturer;
      var model = androidInfo.model;
      version = release.toString();
      print('Android $release (SDK $sdkInt), $manufacturer $model');
      // Android 9 (SDK 28), Xiaomi Redmi Note 7
    }
    
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      var systemName = iosInfo.systemName;
      version = iosInfo.systemVersion;
      var name = iosInfo.name;
      var model = iosInfo.model;
      print('$systemName $version, $name $model');
      // iOS 13.1, iPhone 11 Pro Max iPhone
    }
    
    final Email email = Email(
      body: '''
      $description
      
      ----------------------------------------------
      
      ${Platform.isAndroid ? 'Android' : 'iPhone'} Version : $version
      App Version : 1.0
      Device Model : ${Platform.isIOS ? 'iPhone' : 'Android'}

      
      Regards
      ''',
      subject: '$reason in $os',
      recipients: ['kingdragon2108@gmail.com','digitsu@ymail.com'],
      // cc: ['cc@example.com'],
      // bcc: ['bcc@example.com'],
      // attachmentPath: '/path/to/attachment.zip',
      isHTML: false,
    );
    try {
      await FlutterEmailSender.send(email);
      return true;
    } catch (e) {
      return false;
    }
  }
}