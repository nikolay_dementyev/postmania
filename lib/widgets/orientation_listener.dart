import 'package:flutter/material.dart';

class OrientationListener extends StatefulWidget {

  final Widget child;
  final ValueChanged<bool> onOrientationChanged;
  
  const OrientationListener({Key key, this.child, this.onOrientationChanged}) : super(key: key);
  
  @override
  _OrientationListenerState createState() => _OrientationListenerState();
}

class _OrientationListenerState extends State<OrientationListener> {
  
  Orientation _orientation;
  
  @override
  void initState() {
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: ((context, orientation) {
        if(_orientation == null) {
          _orientation = orientation;
          // if(widget.onOrientationChanged != null) {
          //   widget.onOrientationChanged(orientation == Orientation.portrait);
          // }
        } else if(orientation != _orientation) {
          if(widget.onOrientationChanged != null) {
            widget.onOrientationChanged(orientation == Orientation.portrait);
          }
          _orientation = orientation;
        }
        return widget.child ?? Container();
      }),
    );
  }
}