import 'package:firebase_uploading/widgets/async_network_image.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:neeko/neeko.dart';
import 'package:share/share.dart';

class VideoPlayer extends StatefulWidget {
  
  final String url;
  final String title;
  final String thumbnail;
  
  final NeekoPlayerCreatedCallback onPlayerCreated;
  
  const VideoPlayer({Key key, @required this.url, this.title = '', this.onPlayerCreated, this.thumbnail}) : super(key: key);
  
  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayer> {
  
  VideoControllerWrapper videoControllerWrapper;
  
  bool isLoading = true;
  
  NeekoPlayerController controller;
  
  @override
  void initState() {
    super.initState();
    
    videoControllerWrapper = VideoControllerWrapper(
      DataSource.network(
          widget.url,
          displayName: widget.title)
    );
    
    videoControllerWrapper.addListener(() {
      if(widget.onPlayerCreated != null) {
        widget.onPlayerCreated(controller);
      }
      setState(() {
        isLoading = false;
      });
    });
  }
  
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        NeekoPlayerWidget(
          inFullScreen: true,
          playerOptions: NeekoPlayerOptions(
            useController: true
          ),
          onNeekoPlayerCreated: (controller) {  
            this.controller = controller;
          },
          onSkipPrevious: () {
            // print("skip");
            // videoControllerWrapper.prepareDataSource(DataSource.network(
            //     "http://192.168.208.80/1028589392-preview.mp4",
            //     displayName: "This house is not for sale"));
          },
          onSkipNext: () {
            // videoControllerWrapper.prepareDataSource(DataSource.network(
            //     'http://192.168.208.80/181016_02_Grenada_14.mp4',
            //     displayName: "displayName"));
          },
          videoControllerWrapper: videoControllerWrapper,
          actions: <Widget>[
            IconButton(
                icon: Icon(
                  Mdi.share,
                  color: Colors.white,
                ),
                onPressed: () {
                  Share.share(widget.url);
                })
          ],
        ),
        isLoading 
        ? AsyncNetworkImage(
          photoKey: widget.thumbnail + 'video',
          imgUrl: widget.thumbnail,
        )
        : SizedBox(width: 0, height: 0),
        Center(
          child: isLoading 
          ? CircularProgressIndicator(
            backgroundColor: Colors.white,
          )
          : SizedBox(width: 0, height: 0),
        )
      ],
    );
  }
}