import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AsyncNetworkImage extends StatelessWidget {
  const AsyncNetworkImage(
      {Key key,
      @required this.photoKey,
      this.imgUrl,
      this.width,
      this.height,
      this.onTap,
      this.boxfit = BoxFit.cover, 
      this.placeholder = 'assets/images/placeholder_image.png'})
      : super(key: key);
  
  final double width;
  final double height;
  final Function onTap;
  final String photoKey;
  final BoxFit boxfit;
  final String imgUrl;
  final String placeholder;
  
  @override
  Widget build(BuildContext context) {
    if (imgUrl == null) {
      return GestureDetector(
        onTap: onTap,
        child: Image.asset(
          placeholder,
          width: width,
          height: height,
          fit: boxfit,
        )
      );
    } else {
      if (photoKey == '') {
        return GestureDetector(
          onTap: onTap,
          child: CachedNetworkImage(
            placeholder: (BuildContext context, String url) {
              return CachedNetworkImage(
                width: width,
                height: height,
                fit: boxfit,
                placeholder: (BuildContext context, String url) {
                  return Image.asset(
                    placeholder,
                    width: width,
                    height: height,
                    fit: boxfit,
                  );
                },
                imageUrl: imgUrl,
              );
            },
            width: width,
            height: height,
            fit: boxfit,
            imageUrl: imgUrl,
            errorWidget: (BuildContext context, String url, Object error) {
              return Image.asset(
                placeholder,
                width: width,
                height: height,
                fit: boxfit,
              );
            },
          ),
        );
      } else {
        return GestureDetector(
          onTap: onTap,
          child: Hero(
            tag: '$photoKey$imgUrl',
            child: CachedNetworkImage(
              placeholder: (BuildContext context, String url) {
                return CachedNetworkImage(
                  width: width,
                  height: height,
                  fit: boxfit,
                  placeholder: (BuildContext context, String url) {
                    return Image.asset(
                      placeholder,
                      width: width,
                      height: height,
                      fit: boxfit,
                    );
                  },
                  imageUrl: imgUrl,
                );
              },
              width: width,
              height: height,
              fit: boxfit,
              imageUrl: imgUrl,
              errorWidget: (BuildContext context, String url, Object error) =>
                  Image.asset(
                placeholder,
                width: width,
                height: height,
                fit: boxfit,
              ),
            ),
          ),
        );
      }
    }
  }
}