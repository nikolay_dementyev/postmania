import 'package:flutter/material.dart';

///     Length of Titles Should be the same as the length of the Children
/// 
///     Usage 
/// 
///     class SettingScreen extends BaseTabBarScreen {
///       @override
///       List<Widget> getChildren() {
///         return [
///           MyOfferWidget(),
///           NewOfferWidget(),
///         ];
///       }
///
///       @override
///       List<String> getTitles() {
///         return [
///           'My Offers',
///           'New Offers'
///         ];
///       }  
///     }

abstract class BaseTabBarScreen extends StatelessWidget {
  
  const BaseTabBarScreen({this.appBar});
  
  List<String> getTitles();
  
  List<Widget> getChildren();
  
  int getInitialIndex() {
    return 0;
  }
  
  AppBar buildAppBar(BuildContext context) {
    return null;
  }
  
  final AppBar appBar;
  
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return DefaultTabController(
      initialIndex: getInitialIndex(),
      length: getTitles().length,
      child: Scaffold(
        backgroundColor: Colors.orange,
        appBar: buildAppBar(context),
        body: Column(
          children: <Widget>[
             TabBar(
              tabs: List.generate(getTitles().length, (int index) {
                return Tab(text: getTitles()[index]);
              }),
              labelColor: Colors.white,
              labelStyle: TextStyle(fontSize: 16, fontFamily: 'Nexabold'),
              indicatorColor: theme.accentColor,
              unselectedLabelColor: Colors.orange[200],
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
                 child: TabBarView(
                  children: getChildren()
                 ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
