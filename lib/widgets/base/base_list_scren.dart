import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BaseListScreen extends StatelessWidget {
  
  int getCount();
  
  Widget buildListItem(BuildContext context, int index);
  
  void onSearch(String query, BuildContext context) {}
  
  AppBar buildAppBar(BuildContext context) {
    return null;
  }
  
  void onBack(BuildContext context) {
    Navigator.pop(context);
  }
  
  bool allowScroll() => true;
  
  bool showSearchBar() => true;
  
  void onSelected(BuildContext context, int index) {}
  
  void onLongPressed(BuildContext context, int index) {}
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      appBar: buildAppBar(context),
      backgroundColor: Colors.transparent,
      body: Column(
        children: <Widget>[
          showSearchBar() 
          ? SearchBar(onQuery: (query) {
            onSearch(query, context);
          }, onBack: () {
            onBack(context);
          })
          : Container(),
          
           BlocBuilder<LoadBloc, LoadState>(
            builder: (context, state) {
              if(state is Loading) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: LoadingBar(),
                );
              } else {
                return Container();
              }
            },
          ),
          Expanded(
            child: CupertinoScrollbar(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: getCount(),
                itemBuilder: (context, int index) => InkWell(onTap: () {
                  onSelected(context, index);
                }, onLongPress: () {
                  onLongPressed(context, index);
                }, child: buildListItem(context, index)),
              ),
            ),
          )
        ],
      ),
    );
  }
}