import 'package:flutter/material.dart';

class Disable extends StatelessWidget {

  final Widget child;
  final bool isDisabled;
  const Disable({Key key, this.child, this.isDisabled = true}) : super(key: key);
 
  Widget build(BuildContext context) {
    return AbsorbPointer(
      absorbing: isDisabled,
      child: Opacity(
        opacity: isDisabled ? 0.5 : 1.0,
        child: child,
      ),
    );
  }
}