import 'package:firebase_uploading/widgets/diable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/Picker.dart';

class CustomPickerController {
  _CustomPickerState _customPickerState;

  CustomPickerController(_CustomPickerState _customPickerState) {
    this._customPickerState = _customPickerState;
  }

  setSuggestions(List<String> suggestions) {
    _customPickerState.setSuggestions(suggestions);
  }
}

typedef CustomPickerCreated(CustomPickerController controller);

class CustomPicker extends StatefulWidget {
  
  final ValueChanged<String> onSubmit;
  final List<String> suggestions;
  final String hint;
  final Widget right;
  final String intialValue;
  final CustomPickerCreated onPickerCreated;

  CustomPicker({Key key, this.onSubmit, this.suggestions, this.hint = 'Enter Suggesstion', this.right, this.intialValue, this.onPickerCreated}) : super(key: key);
  
  @override
  _CustomPickerState createState() => _CustomPickerState();
}

class _CustomPickerState extends State<CustomPicker> {
  
  TextEditingController _controller = TextEditingController();
  
  List<String> _suggestions;
  CustomPickerController _pickerController;
  
  int index;
  
  setSuggestions(List<String> suggestions) {
    setState(() {
      _suggestions = suggestions;
      if(_suggestions != null && _suggestions.isNotEmpty) {
        index = 0;
        _controller.text = _suggestions.first;
        widget.onSubmit(_suggestions.first);
      } else {
        index = null;
        _controller.text = '';
      }
    });
  }
  
  _onPicked(String picked) {
    _controller.text = picked;
      if(widget.onSubmit != null) {
        widget.onSubmit(picked);
      }
  }
  
  @override
  void initState() {
    super.initState();
    
    _pickerController = CustomPickerController(this);
    if(widget.onPickerCreated != null) {
      widget.onPickerCreated(_pickerController);
    }

    if(widget.suggestions.isEmpty) return;
    
    _suggestions = widget.suggestions;
    
    if(widget.intialValue != null) {
      if(_suggestions.contains(widget.intialValue)) {
        
        _controller.text = widget.intialValue;
        index = _suggestions.indexWhere((item) => item == widget.intialValue);
        if(widget.onSubmit != null) {
          widget.onSubmit(widget.intialValue);
        }
      }
    } else {
      _controller.text = _suggestions.first;
      index = 0;
      if(widget.onSubmit != null) {
        widget.onSubmit(_suggestions.first);
      }
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Disable(
      isDisabled: _suggestions == null || _suggestions.isEmpty,
      child: InkWell(
        onTap: () {
          Picker(
            selecteds: [index],
            adapter: PickerDataAdapter<String>(
              pickerdata: [_suggestions],
              isArray: true
            ), 
            onConfirm: (picker, selecteds) {
              index = selecteds.first;
              _onPicked(_suggestions[selecteds.first]);
            }
          ).showModal(context);
        },
        child: AbsorbPointer(
          absorbing: true,
          child: TextFormField(
            style: TextStyle(fontFamily: 'Nexabold'),
            decoration: InputDecoration(
                  hintText: widget.hint,
                  enabled: false,
                  suffixIcon: widget.right != null ? null : Icon(Icons.arrow_drop_down),
                  border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ), errorStyle: TextStyle(height: 0, )),
            controller: _controller,
          ),
        ),
      ),
    );
  }
}