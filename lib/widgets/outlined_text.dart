import 'package:flutter/material.dart';

class OutlinedText extends StatelessWidget {

  final String text;
  final Color insideColor;
  final Color outsideColor;
  final double size;
  
  const OutlinedText({Key key, this.text, this.insideColor, this.outsideColor, this.size}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Text(
          text,
          style: TextStyle(
            fontSize: size,
            foreground: Paint()
              ..style = PaintingStyle.stroke
              ..strokeWidth = 2
              ..color = outsideColor,
          ),
        ),
        Text(
          text,
          style: TextStyle(
            fontSize: size,
            color: insideColor,
          ),
        ),
      ],
    );
  }
}