import 'package:flutter/material.dart';
import 'collapsible_panel.dart';

class ExPansionPanel extends StatefulWidget {
  
  final Widget titleWidget;
  final String title;
  final Widget child;
  final bool isExpanded;
  const ExPansionPanel({Key key, @required this.child, this.title, this.titleWidget, this.isExpanded = false}) : super(key: key);
  
  @override
  _InsuranceInformatinPanelState createState() => _InsuranceInformatinPanelState();
}

class _InsuranceInformatinPanelState extends State<ExPansionPanel> {
  
  bool expaned = false;
  
  @override
  void initState() {
    super.initState();
    
    expaned = widget.isExpanded;
  }
  
  @override
  Widget build(BuildContext context) {
    return CustomExpansionPanel(
      onExpansionChanged: (value) {
        setState(() {
          expaned = value;
        });
      },
      title: widget.titleWidget ?? Container(
        decoration: BoxDecoration(
          color: Color(0xFFDDDDDD),
          border: Border.fromBorderSide(BorderSide(
            color: Colors.white,
            width: 1.0
          ))
        ),
        padding: EdgeInsets.symmetric(vertical: 3),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(children: <Widget>[
            Text(widget.title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontFamily: 'Nexabold')),
            Spacer(),
            // Icon(expaned ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
            Icon(expaned ? Icons.keyboard_arrow_right : Icons.keyboard_arrow_right),
            const SizedBox(width: 10)
          ]),
        ),
      ),
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
          child: widget.child,
        )
      ],
    );
  }
}
