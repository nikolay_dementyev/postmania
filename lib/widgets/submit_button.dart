import 'package:flutter/material.dart';
import 'package:firebase_uploading/blocs/index.dart';
import 'package:firebase_uploading/widgets/index.dart';

class SubmitButton extends StatelessWidget {
  
  final String title;
  final VoidCallback onPressed;
  final Color color;
  const SubmitButton({Key key, @required this.title, this.onPressed, this.color = Colors.white}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SubmitBloc, SubmitState>(
      builder: (context, state) {
        return InkWell(
          onTap: state is Submitting ? null : onPressed,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 50),
            constraints: BoxConstraints(
              minWidth: 239,
              maxWidth: 350
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              color: color
            ),
            height: 50,
            child: Center(
              child: state is Submitting ? LoadingBar() : Text(title, style: TextStyle(fontSize: 20, fontFamily: 'Nexabold', color: Colors.black54)),
            ),
          ),
        );
      },
    );
  }
}