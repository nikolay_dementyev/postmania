import 'package:firebase_uploading/singletons/global.dart';
import 'package:flutter/material.dart';

class TextArea extends StatefulWidget {
  
  final ValueChanged<String> onChanged;
  final String initialValue;
  final String hint;
  final String label;
  final bool showLabel;
  final double minHeight;
  final bool showTextCount;
  final int maxCount;
  
  const TextArea({Key key, this.initialValue = '', this.onChanged, this.hint = '', this.showLabel = true, this.maxCount = 300, this.showTextCount = true, this.minHeight = 100, this.label = 'Description'}) : super(key: key);
  @override
  _TextAreatState createState() => _TextAreatState();
}

class _TextAreatState extends State<TextArea> {
  
  int textCount;
  
  @override
  void initState() {
    super.initState();
    
    textCount = widget.initialValue.length;
  }
  
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, top: 0, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          widget.showLabel 
          ? Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(  
              child: Align(alignment: Alignment.centerLeft,child: Text(widget.label, style: TextStyle(color: Colors.blueGrey, fontWeight: FontWeight.bold, fontSize: 16)))
            ),
          )
          : Container(),
          Container( 
            padding: EdgeInsets.only(bottom: 5, left: 10, right: 10),
            // height: 140,
            constraints: BoxConstraints(
              minHeight: widget.minHeight
            ),
            decoration: BoxDecoration(
              border: Border.all(width: 2, color: textCount == widget.maxCount ? Colors.red : Color(0xFFE5E6EA)),
              color: Colors.white,
            ),             
            child: TextFormField(
              maxLines: null,
              initialValue: widget.initialValue,
              maxLength: widget.maxCount,
              decoration: InputDecoration(
                border: InputBorder.none,  
                hintText: widget.hint, 
                counterText: '',    
                counter: null
              ),
              onChanged: (value) {

               if(value.length >= widget.maxCount) {
                 Global().showToastMessage('${widget.label} should be smaller than ${widget.maxCount}');
               }
              
               if(widget.onChanged != null) {
                  widget.onChanged(value);
                }
                setState(() {
                  textCount = value.length;
                });
              }, 
              style: TextStyle(color: Colors.black, height: 1.3, fontSize: 18),
            ),
          ),
          widget.showTextCount ? Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Align(
              alignment: Alignment.centerRight,
              child: Text('$textCount of ${widget.maxCount} characters'),
            ),
          ) : Container(),
        ],
      ),
    );  
  }
} 