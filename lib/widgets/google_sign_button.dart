import 'package:flutter/material.dart';
import 'package:firebase_uploading/blocs/index.dart';

class GoogleSignButton extends StatelessWidget {
  
  final VoidCallback onPressed;
  
  const GoogleSignButton({Key key, this.onPressed}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap:  onPressed,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 50),
        constraints: BoxConstraints(
          minWidth: 239,
          maxWidth: 350
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          color: Colors.white
        ),
        height: 50,
        child: Center(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset('assets/images/icon_google.png', width: 30, height: 30),
              const SizedBox(width: 14),
              Text('Sign with google', style: TextStyle(fontSize: 20, fontFamily: 'Nexabold', color: Colors.black54))
            ],
          ),
        ),
      ),
    );
  }
}