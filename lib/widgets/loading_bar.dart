import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoadingBar extends StatelessWidget {
   @override
  Widget build(BuildContext context) {
    return CupertinoActivityIndicator(radius: 15);
  }
}