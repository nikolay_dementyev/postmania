import 'package:flutter/material.dart';

class InfoItem extends StatelessWidget {

  final bool isAutoLocation;
  final VoidCallback onLocationClicked;
  final String info;
  
  const InfoItem({Key key, this.info, this.isAutoLocation = false, this.onLocationClicked}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5)),
        border: Border.all(color: Colors.grey)
      ),
      padding: EdgeInsets.symmetric(horizontal: 10,vertical: 15),
      child: Row(
        children: <Widget>[
          Expanded(child: Text(info, style: TextStyle(fontFamily: 'Nexabold', fontSize: 16))),
          InkWell(onTap: () {
            if(isAutoLocation) {
              onLocationClicked();
            }
          }, child: Icon(isAutoLocation ? Icons.near_me : Icons.arrow_drop_down, color: isAutoLocation ? Colors.orange : Colors.grey,))
        ],
      ),
    );
  }
}