import 'package:flutter/material.dart';
import 'flutter_typeahead.dart';

class TypeAhead extends StatefulWidget {

  final VoidCallback onLocationClicked;
  final bool isAutoAddress;
  final ValueChanged<String> onSubmit;
  final List<String> suggestions;
  final String hint;
  final TextEditingController controller;
  const TypeAhead({Key key, this.onSubmit, this.suggestions, this.hint = 'Enter Suggesstion', this.isAutoAddress = false, this.onLocationClicked, this.controller}) : super(key: key);
  
  Future<List<String>> getSuggesstions(String pattern) async {
    List<String> filteredList = List.from(suggestions);
    filteredList.retainWhere((item) => item.toLowerCase().startsWith(pattern.toLowerCase()));
    return filteredList;
  }
  
  @override
  _TypeAheadState createState() => _TypeAheadState();
}

class _TypeAheadState extends State<TypeAhead> {
  
  TextEditingController _controller;
  
  @override
  void initState() {
    super.initState();

    _controller = widget.controller ?? TextEditingController();
  }
  
  @override
  Widget build(BuildContext context) {
    return TypeAheadFormField(
      textFieldConfiguration: TextFieldConfiguration(
          decoration: InputDecoration(
            hintText: widget.hint,
            suffixIcon: InkWell(onTap: widget.onLocationClicked != null ? widget.onLocationClicked : null, child: Icon(widget.isAutoAddress ? Icons.near_me : Icons.arrow_drop_down, color: widget.isAutoAddress ? Colors.orange : Colors.grey,)),
            border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5)),
          ), errorStyle: TextStyle(height: 0, )),
          controller: _controller,
          style: TextStyle(fontSize: 18, color: Colors.black87, fontFamily: 'Nexabold')
      ),
      
      suggestionsCallback: (pattern) async {
        return await widget.getSuggesstions(pattern);
      },
      itemBuilder: (context, String suggestion) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(suggestion, style: TextStyle(fontSize: 16, fontFamily: 'Nexabold')),
              ),
            ],
          )
        );
      },
      transitionBuilder: (context, suggestionBox, controller) {
        return suggestionBox;
      },
      onSuggestionSelected: (String suggestion) {
        _controller.text = suggestion;
        if(widget.onSubmit != null) {
          widget.onSubmit(suggestion);
        }
      },
    );
  }
}