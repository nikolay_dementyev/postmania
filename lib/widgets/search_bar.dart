import 'package:firebase_uploading/singletons/index.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  
  final ValueChanged<String> onQuery;
  final VoidCallback onBack;
  
  SearchBar({Key key, this.onQuery, this.onBack}) : super(key: key);
  
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  
  final TextEditingController controller = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.orange, width: 7.0)
      ),
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          const SizedBox(width: 5),
          Expanded(
            child: TextField(
              autofocus: true,
              controller: controller,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: 'Search...'
              ),
              onEditingComplete: () {
                if(widget.onQuery != null) {
                  widget.onQuery(controller.text);
                }
                Global().hideSoftKeyboard(context);
              },
            ),
          ),
          const SizedBox(width: 5),
          InkWell(onTap: () {
            controller.text = '';
            if(widget.onQuery != null) {
              widget.onQuery('');
            }
          }, child: Icon(Icons.close, color: Colors.black))
        ]
      ),
    );
  }
}