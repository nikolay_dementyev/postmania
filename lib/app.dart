import 'package:firebase_uploading/app/login/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'app/home/ui/home_page.dart';
import 'app/splash/splash_page.dart';

class MyApp extends StatelessWidget {
  
  final GlobalKey<NavigatorState> navigatorKey;
  
  MyApp({Key key, @required this.navigatorKey}) : super(key: key);
  
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      theme: ThemeData(fontFamily: 'Nexalight', appBarTheme: AppBarTheme(
        color: Colors.orange,
        textTheme: TextTheme(
          title: TextStyle(fontFamily: 'Nexabold', fontSize: 20)
        ),
      ), backgroundColor: Colors.orange, scaffoldBackgroundColor: Colors.white),
      home: SplashPage(),
      // home: VideoTest(),
      routes: {
        '/home': (context ) => HomePage(),
        '/login': (context) => LoginPage()
      },
    );
  }
}