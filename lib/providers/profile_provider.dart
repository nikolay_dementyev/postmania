import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:flutter/cupertino.dart';

class ProfileProvider extends ChangeNotifier {
  
  final PostRepository repository = PostRepository();
  
  List<String> countries = [];
  List<String> cities = [];
  
  String curCountry;
  String curCity;
  
  getInformation(Profile profile) async {
    
    cities = [];
    
    curCountry = profile.countryName;
    curCity = profile.cityName;
    
    countries = await repository.getCoutries();
    countries.remove(curCountry);
    countries.insert(0, curCountry);
    cities = [curCity];
    notifyListeners();
    
    List<String> states = await repository.getRegions(curCountry);
    
    for(int i = 0; i < states.length; i++) {
      repository.getCities(states[i]).then((cityList) {
        print('${cities.length} cities gget');
        if(cities != null) {
          this.cities.addAll(cityList);
          if(cities.contains(curCity)) {
            cities.remove(curCity);
            cities.insert(0, curCity);
          } 
          notifyListeners();
        }
      });
    }
  }
  
  selectCountry(String country) async {
    
    if(country == this.curCountry) return;
    
    cities = [];
    notifyListeners();
    curCountry = country;
    List<String> states = await repository.getRegions(curCountry);
    
    for(int i = 0; i < states.length; i++) {
      repository.getCities(states[i]).then((cityList) {
        print('${cities.length} cities gget');
        if(cities != null) {
          
          this.cities.addAll(cityList);
          if(cities.contains(curCity)) {
            cities.remove(curCity);
            cities.insert(0, curCity);
          } 
          notifyListeners();
        }
      });
    }
  }
}