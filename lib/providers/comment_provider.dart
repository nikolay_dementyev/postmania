import 'package:firebase_uploading/blocs/authentication/authentication_bloc.dart';
import 'package:firebase_uploading/models/comment.dart';
import 'package:firebase_uploading/models/index.dart';
import 'package:firebase_uploading/utils/firebase/index.dart';
import 'package:flutter/cupertino.dart';

class CommentProvider extends ChangeNotifier {
  
  final AuthenticationBloc authenticationBloc;
  
  String _postId;
  Comment _comment;
  
  List<Comment> comments = [];
  
  List<Profile> _userDetails = [];
  
  CommentProvider(this.authenticationBloc);
  
  fetchComments(String postId) async {

    comments = [];
    notifyListeners();
    
    this._postId = postId;
    
    List<Comment> allComments = await FireStore.fetchComments(postId);
    if(allComments == null) return;
    
    for(int i = 0; i < allComments.length; i++) {
      
      /// Get Commenter Detail
      String userId = allComments[i].userId;
      
      /// Search in local user list
      Profile profile;
      try {
        profile = _userDetails.firstWhere((item) => item.userId == userId, orElse: null);
      } catch (e) {
        profile = null;
      }
      
      /// If not found in local list, fetch from remote database
      if(profile == null) {
        profile = await FireStore.getUserDetail(userId);
        if(profile == null) return;
         
        _userDetails.insert(0, profile);
      }
      
      allComments[i].avatar = profile.avatarUrl;
      allComments[i].name = profile.userName;
      
      if(allComments[i].parentId == null) {
        comments.add(allComments[i]);
      } else {
        try {
          Comment comment = comments.firstWhere((item) => item.id == allComments[i].parentId, orElse: null);
          if(comment != null) {
            comment.replyComments.add(allComments[i]);
          }
        } catch (e) {

        }
      }
    }
    for(int i = 0; i < comments.length; i++) {
      comments[i].replyComments = comments[i].replyComments.reversed.toList();
    }
    comments = comments.reversed.toList();
    notifyListeners();
  }
  
  _addComment(Comment comment) async {
    Comment newComment = await FireStore.addComment(comment);
    if(newComment == null) return;
    
    if(newComment.parentId == null) {
      comments.insert(0, newComment);
    } else {
      int parentIndex = comments.indexWhere((item) => item.id == newComment.parentId);
      List<Comment> replyComments = comments[parentIndex].replyComments;
      replyComments.insert(0, newComment);
    }
    notifyListeners();
  }
  
  _updateComment(Comment comment) async {
    Comment updatedComment = await FireStore.updateComment(comment);
    if(updatedComment == null) return;
    
    if(updatedComment.parentId == null) {
      int index = comments.indexWhere((item) => item.id == updatedComment.id);
      comments[index] = updatedComment;
    } else {
      int parentIndex = comments.indexWhere((item) => item.id == updatedComment.parentId);
      List<Comment> replyComments = comments[parentIndex].replyComments;
      int index = replyComments.indexWhere((item) => item.id == updatedComment.id);
      replyComments[index] = updatedComment;
    }
    notifyListeners();
  }
  
  /// set selected comment
  selectComment(Comment comment) {
    _comment = comment;
  }
  
  /// deselect comment
  deselectComment() {
    _comment = null;
  }
  
  /// insert comment and replies
  addNewComment(String commentText) {
   
    Profile profile = authenticationBloc.state.profile;
    Comment comment = Comment(userId: profile.userId, postId: _postId, comment: commentText);
    comment.name = profile.userName;
    comment.avatar = profile.avatarUrl;
    
    // comment.parentId = commentId;
    if(_comment != null) {
      comment.parentId = _comment.parentId ?? _comment.id;
      String userId = _comment.userId;
      Profile userProfile = _userDetails.firstWhere((item) => item.userId == userId);
      comment.oppositeName = userProfile.userName;
    }
    
    _comment = null;
    _addComment(comment);
  }
  
  like(Comment comment, String userId) {
    
    List<String> likedUsers = comment.likedUsers;
    List<String> dislikedUsers = comment.dislikedUsers;
     
    if(likedUsers.contains(userId)) {
      likedUsers.remove(userId);
    } else {
      likedUsers.add(userId);
      if(dislikedUsers.contains(userId)) {
        dislikedUsers.remove(userId);
      }
    }

    _updateComment(comment);
  }
  
  dislike(Comment comment, String userId) {
    
    List<String> likedUsers = comment.likedUsers;
    List<String> dislikedUsers = comment.dislikedUsers;

    if(dislikedUsers.contains(userId)) {
      dislikedUsers.remove(userId);
    } else {
      dislikedUsers.add(userId);
      if(likedUsers.contains(userId)) {
        likedUsers.remove(userId);
      }
    }
    _updateComment(comment);
  }
}