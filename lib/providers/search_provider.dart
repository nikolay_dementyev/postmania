import 'package:firebase_uploading/app/post/bloc/bloc.dart';
import 'package:firebase_uploading/app/post/bloc/post_repository.dart';
import 'package:firebase_uploading/blocs/authentication/bloc.dart';
import 'package:firebase_uploading/blocs/load/bloc.dart';
import 'package:firebase_uploading/blocs/load/load_bloc.dart';
import 'package:flutter/material.dart';

class SearchProvider extends ChangeNotifier {

  final AuthenticationBloc authenticationBloc;
  final LoadBloc loadBloc;
  final PostRepository repository = PostRepository();

  SearchProvider({this.authenticationBloc, @required this.loadBloc});
   
  List<Post> _postList;
  
  List<Post> get postList => _postList;
  
  fetchPosts(String query) async {
    loadBloc.add(Load());
    _postList = await repository.fetchPosts(query);
    if(_postList != null) {
      String userId = authenticationBloc.state.profile.userId;
      for(int i = 0; i < _postList.length; i++) {
        Post post = _postList[i];
        if(post.posterId == userId) {
          post.me = true;
        } 
      }
      notifyListeners();
      loadBloc.add(LoadSucceeded());
    } else {
      loadBloc.add(LoadFailed());
    }
  }
}